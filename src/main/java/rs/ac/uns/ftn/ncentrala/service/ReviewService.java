package rs.ac.uns.ftn.ncentrala.service;

import rs.ac.uns.ftn.ncentrala.model.Review;

import java.util.List;

public interface ReviewService {

    Review save(Review review);

    Review findById(Long id);

    List<Review> findBySciencePaperId(Long id);

    List<Review> findByReviewerId(Long id);

    void deleteAll(Iterable<Review> iterable);
}
