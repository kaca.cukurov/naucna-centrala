package rs.ac.uns.ftn.ncentrala.search;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import rs.ac.uns.ftn.ncentrala.model.*;
import rs.ac.uns.ftn.ncentrala.repository.elasticsearch.ReviewerESRepository;
import rs.ac.uns.ftn.ncentrala.repository.elasticsearch.SciencePaperESRepository;
import rs.ac.uns.ftn.ncentrala.service.ReviewerESService;
import rs.ac.uns.ftn.ncentrala.service.SciencePaperESService;
import rs.ac.uns.ftn.ncentrala.service.SciencePaperService;
import rs.ac.uns.ftn.ncentrala.web.dto.SearchReviewerESDto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.geoDistanceQuery;
import static org.elasticsearch.index.query.QueryBuilders.moreLikeThisQuery;


@Service
public class ReviewerSearch {

    @Autowired
    SciencePaperESService sciencePaperESService;

    @Autowired
    SciencePaperESRepository repository;

    @Autowired
    ReviewerESRepository reviewerESRepository;

    @Autowired
    SciencePaperService sciencePaperService;

    @Autowired
    ReviewerESService reviewerESService;

    private final String getLocation = "https://nominatim.openstreetmap.org/search?format=json&";
    private final String fbclid = "IwAR22I8A1HQ4VE78RaZSHp1sfTPAjiwbol_PMorlzaY9ZKlpXTrWaI3ALpBU";

    public List<SearchReviewerESDto> moreLikeThisSearch(String title) throws Exception{

        List<SearchReviewerESDto> foundReviewersEs = new ArrayList<>();
        SciencePaper paper = this.sciencePaperService.getByTitle(title);

        String paperText = getText(paper.getFilePath());
        String[] fields = {"text"};
        String[] texts = {paperText};

        MoreLikeThisQueryBuilder queryBuilder1 = moreLikeThisQuery(fields, texts, null)
                .minTermFreq(1)
                .minDocFreq(1);

        for(SciencePaperES paperES: this.repository.search(queryBuilder1)){
            SciencePaper sciencePaper = this.sciencePaperService.getOne(paperES.getId());
            for(Account reviewer: sciencePaper.getReviewers()){
                foundReviewersEs.add(convertReviewerEsFromReviewer(this.reviewerESService.findOne(reviewer.getId().toString())));
            }
        }

        return foundReviewersEs;
    }


    public List<SearchReviewerESDto> geoSearch(String title){

        List<SearchReviewerESDto> foundReviewersEs = new ArrayList<>();
        SciencePaper paper = this.sciencePaperService.getByTitle(title);

        BoolQueryBuilder finalQuery = QueryBuilders.boolQuery();

        for(Coauthor coauthor : paper.getCoauthors()){

            String request = getLocation + "city=" + coauthor.getCity() + "&country=" + coauthor.getCountry() + "&fbclid=" + fbclid;

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> result = restTemplate.getForEntity(request, String.class);

            Integer latInt = result.toString().indexOf("\"lat\":");
            Integer lonInt = result.toString().indexOf("\"lon\":");
            String lat = result.toString().substring(latInt+7, latInt+16);
            String lon = result.toString().substring(lonInt+7, lonInt+16);
            Double latitude = Double.parseDouble(lat);
            Double longitude = Double.parseDouble(lon);


            GeoDistanceQueryBuilder queryBuilder = geoDistanceQuery("location")
                    .point(latitude, longitude)
                    .distance(100, DistanceUnit.KILOMETERS);

            finalQuery.mustNot(queryBuilder);
        }

        for(ReviewerES reviewer: this.reviewerESRepository.search(finalQuery)){
            foundReviewersEs.add(convertReviewerEsFromReviewer(reviewer));
        }

        return foundReviewersEs;
    }



    public static SearchReviewerESDto convertReviewerEsFromReviewer(ReviewerES reviewer) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(reviewer, SearchReviewerESDto.class);
    }

    private String getText(String filePath){
        String parsedText = null;
        try {

            File f = new File(filePath);
            PDFParser parser = new PDFParser(new org.apache.pdfbox.io.RandomAccessFile(f, "r"));
            parser.parse();

            COSDocument cosDoc = parser.getDocument();
            PDFTextStripper pdfStripper = new PDFTextStripper();
            PDDocument pdDoc = new PDDocument(cosDoc);
            parsedText = pdfStripper.getText(pdDoc);
            pdDoc.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return parsedText;
    }
}
