package rs.ac.uns.ftn.ncentrala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Coauthor;
import rs.ac.uns.ftn.ncentrala.repository.jpa.CoauthorRepository;

import java.util.List;

@Service
public class CoauthorServiceImpl implements CoauthorService {

    @Autowired
    CoauthorRepository coauthorRepository;

    @Override
    public Coauthor save(Coauthor coauthor){
        return this.coauthorRepository.save(coauthor);
    }

    @Override
    public void deleteAll(Iterable<Coauthor> iterable){
        this.coauthorRepository.deleteInBatch(iterable);
    }

    @Override
    public List<Coauthor> findBySciencePaperId(Long id){
        return this.coauthorRepository.findBySciencePaperId(id);
    }
}
