package rs.ac.uns.ftn.ncentrala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Comment;
import rs.ac.uns.ftn.ncentrala.repository.jpa.CommentRepository;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public Comment save(Comment comment){
        return this.commentRepository.save(comment);
    }

    @Override
    public Comment findOne(Long id){
        return this.commentRepository.getOne(id);
    }

    @Override
    public Comment findBySciencePaperId(Long id){
        return this.commentRepository.findBySciencePaperId(id);
    }
}
