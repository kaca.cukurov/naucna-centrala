package rs.ac.uns.ftn.ncentrala.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import rs.ac.uns.ftn.ncentrala.model.enumeration.BillingType;
import rs.ac.uns.ftn.ncentrala.model.enumeration.ScienceField;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Journal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "journal_id")
    private Long id;

    @Column(unique = true)
    private Long issn;

    @Column(nullable = false)
    private String title;

    @ElementCollection(targetClass = ScienceField.class)
    @JoinTable(name = "JournalScienceFields", joinColumns = @JoinColumn(name = "journal_id"))
    @Column(name = "science_fields", nullable = false)
    @Enumerated(EnumType.STRING)
    private Collection<ScienceField> scienceFieldList;

    @Column(nullable = false)
    private BillingType billingType;

    @OneToOne
    private ScientificCommittee scientificCommittee;

    @OneToMany(mappedBy = "reviewingJournals", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Account> reviewers;
}

