package rs.ac.uns.ftn.ncentrala.search;

import lombok.NoArgsConstructor;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Coauthor;
import rs.ac.uns.ftn.ncentrala.model.SciencePaperES;
import rs.ac.uns.ftn.ncentrala.repository.elasticsearch.SciencePaperESRepository;
import rs.ac.uns.ftn.ncentrala.web.dto.SearchSciencePaperDto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@NoArgsConstructor
public class ResultRetrieverService {

    @Autowired
    SciencePaperESRepository repository;

    @Autowired
    ElasticsearchTemplate template;


    public List<SearchSciencePaperDto> getResults(org.elasticsearch.index.query.QueryBuilder query) {
        if (query == null) {
            return null;
        }
        Map<SciencePaperES, SearchSciencePaperDto> highlights = new HashMap<>();

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(query)
                .withHighlightFields(new HighlightBuilder.Field("text")).build();

        Page<SciencePaperES> result = template.queryForPage(searchQuery, SciencePaperES.class, new SearchResultMapper() {
            @Override
            public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> aClass, Pageable pageable) {
                List<SciencePaperES> chunk = new ArrayList<SciencePaperES>();
                for (SearchHit searchHit : response.getHits()) {
                    if (response.getHits().getHits().length <= 0) {
                        return null;
                    }
                    SciencePaperES paperES = new SciencePaperES();
                    paperES.setId((String) searchHit.getSourceAsMap().get("id"));
                    paperES.setTitle((String) searchHit.getSourceAsMap().get("title"));
                    paperES.setJournalName((String) searchHit.getSourceAsMap().get("journalName"));
                    paperES.setPaperAbstract((String) searchHit.getSourceAsMap().get("paperAbstract"));
                    paperES.setKeyWords((String) searchHit.getSourceAsMap().get("keyWords"));
                    paperES.setScienceField((String) searchHit.getSourceAsMap().get("scienceField"));
                    paperES.setText((String) searchHit.getSourceAsMap().get("text"));
                    paperES.setCoauthors((List<Coauthor>) searchHit.getSourceAsMap().get("coauthors"));

                    StringBuilder stringBuilder = new StringBuilder();
                    if(searchHit.getHighlightFields().get("text") != null){
                        Text[] text = searchHit.getHighlightFields().get("text").fragments();
                        for(Text t : text){
                            stringBuilder.append(t.toString());
                            stringBuilder.append("...");
                        }
                    }
                    SearchSciencePaperDto paperDto = convertSciencePaperESToSearchSciencePaperES(paperES);
                    if(stringBuilder.toString().equals("")){
                        paperDto.setText(paperES.getText().substring(0, 300));
                    }else
                        paperDto.setText(stringBuilder.toString());

                    highlights.put(paperES, paperDto);
                    chunk.add(paperES);
                }
                if (chunk.size() > 0) {
                    return new AggregatedPageImpl<T>((List<T>) chunk);
                }
                return null;
            }
        });

        List<SearchSciencePaperDto> retVal = new ArrayList<>();

        if(result != null){
            for(SciencePaperES paperES: result){
                retVal.add(highlights.get(paperES));
            }
        }

        return retVal;
    }

    public static SearchSciencePaperDto convertSciencePaperESToSearchSciencePaperES(SciencePaperES sciencePaperES) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(sciencePaperES, SearchSciencePaperDto.class);
    }

}
