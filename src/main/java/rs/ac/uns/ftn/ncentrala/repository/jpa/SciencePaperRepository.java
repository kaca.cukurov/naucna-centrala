package rs.ac.uns.ftn.ncentrala.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ncentrala.model.SciencePaper;

public interface SciencePaperRepository extends JpaRepository<SciencePaper, Long> {

    SciencePaper getByTitle(String title);
}
