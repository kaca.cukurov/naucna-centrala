package rs.ac.uns.ftn.ncentrala.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ncentrala.model.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Authority findByType(String type);
}
