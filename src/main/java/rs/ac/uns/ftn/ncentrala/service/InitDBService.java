package rs.ac.uns.ftn.ncentrala.service;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.identity.User;
import org.elasticsearch.common.geo.GeoPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.*;
import rs.ac.uns.ftn.ncentrala.model.enumeration.BillingType;
import rs.ac.uns.ftn.ncentrala.model.enumeration.ScienceField;

import java.util.ArrayList;
import java.util.List;

@Service
public class InitDBService {

    @Autowired
    ReviewerESService reviewerESService;

    @Autowired
    ScientificCommitteeService scientificCommitteeService;

    @Autowired
    JournalService journalService;

    @Autowired
    ReviewerService reviewerService;

    @Autowired
    AuthorityService authorityService;

    @Autowired
    AccountAuthorityService accountAuthorityService;

    @Autowired
    AccountService accountService;


    @Autowired
    IdentityService identityService;

    public void init(){

        //=============== JOURNALS =================
        Journal kacinCasopis = new Journal();
        kacinCasopis.setIssn(123456798L);
        kacinCasopis.setTitle("Kacin casopis");
        List<ScienceField> scienceFieldList = new ArrayList<>();
        scienceFieldList.add(ScienceField.GENERAL_MATHEMATICS);
        scienceFieldList.add(ScienceField.STATISTICS);
        scienceFieldList.add(ScienceField.COMPUTER_SCIENCE);
        kacinCasopis.setScienceFieldList(scienceFieldList);
        kacinCasopis.setBillingType(BillingType.READERS);
        ScientificCommittee scientificCommittee1 = new ScientificCommittee();
        scientificCommittee1 = this.scientificCommitteeService.save(scientificCommittee1);
        kacinCasopis.setScientificCommittee(scientificCommittee1);
        kacinCasopis = this.journalService.save(kacinCasopis);


        Journal djudjinCasopis = new Journal();
        djudjinCasopis.setIssn(26587985321L);
        djudjinCasopis.setTitle("Djudjin casopis");
        List<ScienceField> scienceFieldList2 = new ArrayList<>();
        scienceFieldList2.add(ScienceField.GENERAL_MATHEMATICS);
        scienceFieldList2.add(ScienceField.COMPUTER_SCIENCE);
        scienceFieldList2.add(ScienceField.ROBOTICS_AND_AUTOMATION);
        djudjinCasopis.setScienceFieldList(scienceFieldList2);
        djudjinCasopis.setBillingType(BillingType.AUTHORS);
        ScientificCommittee scientificCommittee2 = new ScientificCommittee();
        scientificCommittee2 = this.scientificCommitteeService.save(scientificCommittee2);
        djudjinCasopis.setScientificCommittee(scientificCommittee2);
        djudjinCasopis = this.journalService.save(djudjinCasopis);


        //=============== AUTHORITY =================
        Authority reviewer = new Authority();
        reviewer.setType("REVIEWER");
        reviewer = this.authorityService.save(reviewer);

        Authority editor = new Authority();
        editor.setType("EDITOR");
        editor = this.authorityService.save(editor);

        Authority chiefEditor = new Authority();
        chiefEditor.setType("CHIEF EDITOR");
        chiefEditor = this.authorityService.save(chiefEditor);

        Authority author = new Authority();
        author.setType("AUTHOR");
        author = this.authorityService.save(author);


        //=============== REVIEWERS =================
        Account reviewer1 = new Account();
        reviewer1.setUsername("kacac");
        reviewer1.setPassword("$2a$04$0bj0yVKs9cqPLeG25cwaeuwAxr3DzuaChyLHvb8fOS/ZywImc8X5K"); //kacac
        reviewer1.setFirstName("Katarina");
        reviewer1.setLastName("Cukurov");
        reviewer1.setEmail("kaca.cukurov@gmail.com");
        reviewer1.setCity("Novi Knezevac");
        reviewer1.setCountry("Srbija");
        reviewer1.setTitle("dipl. ing.");
        List<ScienceField> fields1 = new ArrayList<>();
        fields1.add(ScienceField.COMPUTER_SCIENCE);
        reviewer1.setScienceFieldList(fields1);
        reviewer1 = this.accountService.save(reviewer1);
        AccountAuthority accountAuthority = new AccountAuthority();
        accountAuthority.setAccount(reviewer1);
        accountAuthority.setAuthority(reviewer);
        accountAuthority = this.accountAuthorityService.save(accountAuthority);
        List<AccountAuthority> authorities = new ArrayList<>();
        authorities.add(accountAuthority);
        reviewer1.setAccountAuthorities(authorities);
        reviewer1 = this.accountService.save(reviewer1);


        Account reviewer2 = new Account();
        reviewer2.setUsername("djudja");
        reviewer2.setPassword("$2a$04$TM1x.6TFgorBLE1KKlrrPeQIuxlfAVauIJQ16.6EB4Non9A6sDULm"); //djudja
        reviewer2.setFirstName("Djulinka");
        reviewer2.setLastName("Cukurov");
        reviewer2.setEmail("kaca.cukurov@gmail.com");
        reviewer2.setCity("Novi Sad");
        reviewer2.setCountry("Srbija");
        reviewer2.setTitle("master ing.");
        List<ScienceField> fields2 = new ArrayList<>();
        fields2.add(ScienceField.ROBOTICS_AND_AUTOMATION);
        reviewer2.setScienceFieldList(fields2);
        reviewer2 = this.accountService.save(reviewer2);
        AccountAuthority accountAuthority1 = new AccountAuthority();
        accountAuthority1.setAccount(reviewer2);
        accountAuthority1.setAuthority(reviewer);
        accountAuthority1 = this.accountAuthorityService.save(accountAuthority1);
        List<AccountAuthority> authorities1 = new ArrayList<>();
        authorities1.add(accountAuthority1);
        reviewer2.setAccountAuthorities(authorities1);
        reviewer2 = this.accountService.save(reviewer2);


        Account reviewer3 = new Account();
        reviewer3.setUsername("veki");
        reviewer3.setPassword("$2a$04$kvZUAQ64NWwrXfIxja/sTevj2aFSzS/Z47OY4Most3hgYe3eRLl1a"); //veki
        reviewer3.setFirstName("Verica");
        reviewer3.setLastName("Cukurov");
        reviewer3.setEmail("kaca.cukurov@gmail.com");
        reviewer3.setCity("Vranje");
        reviewer3.setCountry("Srbija");
        reviewer3.setTitle("master ing.");
        List<ScienceField> fields3 = new ArrayList<>();
        fields3.add(ScienceField.MECHANICAL_ENGINEERING);
        reviewer3.setScienceFieldList(fields3);
        reviewer3 = this.accountService.save(reviewer3);
        AccountAuthority accountAuthority3 = new AccountAuthority();
        accountAuthority3.setAccount(reviewer3);
        accountAuthority3.setAuthority(reviewer);
        accountAuthority3 = this.accountAuthorityService.save(accountAuthority3);
        List<AccountAuthority> authorities3 = new ArrayList<>();
        authorities3.add(accountAuthority3);
        reviewer3.setAccountAuthorities(authorities3);
        reviewer3 = this.accountService.save(reviewer3);


        Account reviewer4 = new Account();
        reviewer4.setUsername("dunja");
        reviewer4.setPassword("$2a$04$TZdIyzPIdtAYZ1YGOuY58.l3gcJsqZ3JL9RntH/FV.nZ.SzSf5idG"); //dunja
        reviewer4.setFirstName("Dunja");
        reviewer4.setLastName("Skajovski");
        reviewer4.setEmail("dunja.skajovski@gmail.com");
        reviewer4.setCity("Vranje");
        reviewer4.setCountry("Srbija");
        reviewer4.setTitle("master ing.");
        List<ScienceField> fields4 = new ArrayList<>();
        fields4.add(ScienceField.COMPUTER_SCIENCE);
        reviewer4.setScienceFieldList(fields4);
        reviewer4 = this.accountService.save(reviewer4);
        AccountAuthority accountAuthority4 = new AccountAuthority();
        accountAuthority3.setAccount(reviewer4);
        accountAuthority3.setAuthority(reviewer);
        accountAuthority3 = this.accountAuthorityService.save(accountAuthority4);
        List<AccountAuthority> authorities4 = new ArrayList<>();
        authorities4.add(accountAuthority4);
        reviewer4.setAccountAuthorities(authorities4);
        reviewer4 = this.accountService.save(reviewer4);

        Account reviewer5 = new Account();
        reviewer5.setUsername("lena");
        reviewer5.setPassword("$2a$04$s4MguQLxepS3Wy6dpqqYvOHT4N776qqqSMrGpQEvn82MbzUcZOkkW"); //lena
        reviewer5.setFirstName("Lena");
        reviewer5.setLastName("Skajovski");
        reviewer5.setEmail("lena.skajovski@gmail.com");
        reviewer5.setCity("Novi Knezevac");
        reviewer5.setCountry("Srbija");
        reviewer5.setTitle("master ing.");
        List<ScienceField> fields5 = new ArrayList<>();
        fields5.add(ScienceField.MECHANICAL_ENGINEERING);
        reviewer5.setScienceFieldList(fields5);
        reviewer5 = this.accountService.save(reviewer5);
        AccountAuthority accountAuthority5 = new AccountAuthority();
        accountAuthority5.setAccount(reviewer5);
        accountAuthority5.setAuthority(reviewer);
        accountAuthority5 = this.accountAuthorityService.save(accountAuthority5);
        List<AccountAuthority> authorities5 = new ArrayList<>();
        authorities5.add(accountAuthority5);
        reviewer5.setAccountAuthorities(authorities5);
        reviewer5 = this.accountService.save(reviewer5);


        List<Account> reviewers = new ArrayList<>();
        reviewers.add(reviewer1);
        reviewers.add(reviewer2);
        reviewers.add(reviewer3);
        reviewers.add(reviewer4);
        reviewers.add(reviewer5);
        kacinCasopis.setReviewers(reviewers);
        kacinCasopis = this.journalService.save(kacinCasopis);
        reviewer1.setReviewingJournals(kacinCasopis);
        reviewer2.setReviewingJournals(kacinCasopis);
        reviewer3.setReviewingJournals(kacinCasopis);
        reviewer4.setReviewingJournals(kacinCasopis);
        reviewer5.setReviewingJournals(kacinCasopis);
        this.accountService.save(reviewer1);
        this.accountService.save(reviewer2);
        this.accountService.save(reviewer3);
        this.accountService.save(reviewer4);
        this.accountService.save(reviewer5);

        //=============== EDITORS =================

        Account editor1 = new Account();
        editor1.setUsername("jaca");
        editor1.setPassword("$2a$04$vYUlg9Drp4FMk3M67jP7J.LiIVt7BK0PkyT1bt6D.vfZNQlq/BxQy"); //jaca
        editor1.setFirstName("Jasminka");
        editor1.setLastName("Cukurov");
        editor1.setEmail("kaca.cukurov@gmail.com");
        editor1.setCity("Novi Knezevac");
        editor1.setCountry("Srbija");
        editor1.setTitle("master ing.");
        List<ScienceField> fieldseditor1 = new ArrayList<>();
        fieldseditor1.add(ScienceField.STATISTICS);
        editor1.setScienceFieldList(fieldseditor1);
        editor1 = this.accountService.save(editor1);
        AccountAuthority accountAuthorityeditor1 = new AccountAuthority();
        accountAuthorityeditor1.setAccount(editor1);
        accountAuthorityeditor1.setAuthority(chiefEditor);
        accountAuthorityeditor1 = this.accountAuthorityService.save(accountAuthorityeditor1);
        List<AccountAuthority> authoritieseditor1 = new ArrayList<>();
        authoritieseditor1.add(accountAuthorityeditor1);
        editor1.setAccountAuthorities(authoritieseditor1);
        editor1 = this.accountService.save(editor1);

        Account editor2 = new Account();
        editor2.setUsername("rasa");
        editor2.setPassword("$2a$04$hNNtPGRtXd832EGTZ1A.W.ixvpIVLp2vH/QSmSDYYsByPGtvBUr0K"); //rasa
        editor2.setFirstName("Radovan");
        editor2.setLastName("Cukurov");
        editor2.setEmail("kaca.cukurov@gmail.com");
        editor2.setCity("Novi Knezevac");
        editor2.setCountry("Srbija");
        editor2.setTitle("master ing.");
        List<ScienceField> fieldseditor2 = new ArrayList<>();
        fieldseditor2.add(ScienceField.COMPUTER_SCIENCE);
        editor2.setScienceFieldList(fieldseditor2);
        editor2 = this.accountService.save(editor2);
        AccountAuthority accountAuthorityeditor2 = new AccountAuthority();
        accountAuthorityeditor2.setAccount(editor2);
        accountAuthorityeditor2.setAuthority(editor);
        accountAuthorityeditor2 = this.accountAuthorityService.save(accountAuthorityeditor2);
        List<AccountAuthority> authoritieseditor2 = new ArrayList<>();
        authoritieseditor2.add(accountAuthorityeditor2);
        editor2.setAccountAuthorities(authoritieseditor2);
        editor2 = this.accountService.save(editor2);

        Account editor3 = new Account();
        editor3.setUsername("vlada");
        editor3.setPassword("$2a$04$C/NQX4mhbKFM1YNgID.9BeQUZiU29AytBP1V9PnK5TMHT0cfKed1G"); //vlada
        editor3.setFirstName("Vladimir");
        editor3.setLastName("Cukurov");
        editor3.setEmail("kaca.cukurov@gmail.com");
        editor3.setCity("Novi Knezevac");
        editor3.setCountry("Srbija");
        editor3.setTitle("master ing.");
        List<ScienceField> fieldseditor3 = new ArrayList<>();
        fieldseditor3.add(ScienceField.ROBOTICS_AND_AUTOMATION);
        editor3.setScienceFieldList(fieldseditor3);
        editor3 = this.accountService.save(editor3);
        AccountAuthority accountAuthorityeditor3 = new AccountAuthority();
        accountAuthorityeditor3.setAccount(editor3);
        accountAuthorityeditor3.setAuthority(editor);
        accountAuthorityeditor3 = this.accountAuthorityService.save(accountAuthorityeditor3);
        List<AccountAuthority> authoritieseditor3 = new ArrayList<>();
        authoritieseditor3.add(accountAuthorityeditor3);
        editor3.setAccountAuthorities(authoritieseditor3);
        editor3 = this.accountService.save(editor3);


        //=============== SCIENTIFIC COMITTEE =================
        ScientificCommittee committee = kacinCasopis.getScientificCommittee();
        committee.setChiefEditor(editor1);
        List<Account> editors = new ArrayList<>();
        editors.add(editor2);
        editors.add(editor3);
        committee.setFieldEditors(editors);
        committee = this.scientificCommitteeService.save(committee);
        editor2.setScientificCommittee(committee);
        editor3.setScientificCommittee(committee);
        this.accountService.save(editor2);
        this.accountService.save(editor3);
        kacinCasopis.setScientificCommittee(committee);
        kacinCasopis = this.journalService.save(kacinCasopis);

    }

    public void initES(){

        ReviewerES reviewerES = new ReviewerES();   // Novi Knezevac
        reviewerES.setId("1");
        reviewerES.setFirstName("Katarina");
        reviewerES.setLastName("Cukurov");
        reviewerES.setEmail("kaca.cukurov@gmail.com");
        reviewerES.setLocation(new GeoPoint(46.034467, 20.107853));
        List<String> list = new ArrayList<>();
        list.add(ScienceField.COMPUTER_SCIENCE.toString());
        reviewerES.setScienceFieldList(list);
        this.reviewerESService.save(reviewerES);

        ReviewerES reviewerES1 = new ReviewerES();  // Novi Sad
        reviewerES1.setId("2");
        reviewerES1.setFirstName("Djulinka");
        reviewerES1.setLastName("Cukurov");
        reviewerES1.setEmail("kaca.cukurov@gmail.com");
        reviewerES1.setLocation(new GeoPoint(45.245475, 19.833101));
        List<String> list1 = new ArrayList<>();
        list1.add(ScienceField.ROBOTICS_AND_AUTOMATION.toString());
        reviewerES1.setScienceFieldList(list1);
        this.reviewerESService.save(reviewerES1);

        ReviewerES reviewerES2 = new ReviewerES();  // Vranje
        reviewerES2.setId("3");
        reviewerES2.setFirstName("Verica");
        reviewerES2.setLastName("Cukurov");
        reviewerES2.setEmail("kaca.cukurov@gmail.com");
        reviewerES2.setLocation(new GeoPoint(42.543608, 21.889588));
        List<String> list2 = new ArrayList<>();
        list2.add(ScienceField.MECHANICAL_ENGINEERING.toString());
        reviewerES2.setScienceFieldList(list2);
        this.reviewerESService.save(reviewerES2);

        ReviewerES reviewerES4 = new ReviewerES();  // Vranje
        reviewerES4.setId("4");
        reviewerES4.setFirstName("Dunja");
        reviewerES4.setLastName("Skajovski");
        reviewerES4.setEmail("dunja.skajovski@gmail.com");
        reviewerES4.setLocation(new GeoPoint(42.543608, 21.889588));
        List<String> list4 = new ArrayList<>();
        list4.add(ScienceField.COMPUTER_SCIENCE.toString());
        reviewerES4.setScienceFieldList(list4);
        this.reviewerESService.save(reviewerES4);

        ReviewerES reviewerES5 = new ReviewerES();  // Novi Knezevac
        reviewerES5.setId("5");
        reviewerES5.setFirstName("Lena");
        reviewerES5.setLastName("Skajovski");
        reviewerES5.setEmail("lena.skajovski@gmail.com");
        reviewerES5.setLocation(new GeoPoint(46.034467, 20.107853));
        List<String> list5 = new ArrayList<>();
        list5.add(ScienceField.MECHANICAL_ENGINEERING.toString());
        reviewerES5.setScienceFieldList(list5);
        this.reviewerESService.save(reviewerES5);

    }
}
