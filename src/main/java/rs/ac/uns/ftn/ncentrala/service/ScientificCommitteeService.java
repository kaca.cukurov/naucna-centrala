package rs.ac.uns.ftn.ncentrala.service;

import rs.ac.uns.ftn.ncentrala.model.ScientificCommittee;

public interface ScientificCommitteeService {

    ScientificCommittee save(ScientificCommittee committee);
}
