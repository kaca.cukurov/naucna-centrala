package rs.ac.uns.ftn.ncentrala.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.uns.ftn.ncentrala.model.Account;
import rs.ac.uns.ftn.ncentrala.model.enumeration.ScienceField;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class ReviewerDto {

    private String firstName;
    private String lastName;
    private List<String> scienceFieldList;
    private String username;
    private String email;
    private Long id;

    public ReviewerDto(Account account){
        this.id = account.getId();
        this.email = account.getEmail();
        this.firstName = account.getFirstName();
        this.lastName = account.getLastName();
        this.username = account.getUsername();
        this.scienceFieldList = new ArrayList<>();
        for(ScienceField field: account.getScienceFieldList()){
            this.scienceFieldList.add(field.toString().toLowerCase().replace('_', ' '));
        }
    }
}
