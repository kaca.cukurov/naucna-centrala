package rs.ac.uns.ftn.ncentrala.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import rs.ac.uns.ftn.ncentrala.model.enumeration.ScienceField;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = SciencePaperES.INDEX_NAME, type = SciencePaperES.TYPE_NAME, shards = 1, replicas = 0)
public class SciencePaperES {

    static final String INDEX_NAME = "magazine";
    static final String TYPE_NAME = "paper";

    @Id
    @Field(type = FieldType.Text, index = false, store = true)
    private String id;

    @Field(type = FieldType.Text, store = true)
    private String doi;

    @Field(type = FieldType.Text, store = true)
    private String title;

    @Field(type = FieldType.Text, store = true)
    private String filePath;

    @Field(type = FieldType.Text, store = true)
    private String journalName;

    @Field(type = FieldType.Nested, store = true)
    private List<Coauthor> coauthors = new ArrayList<>();

    @Field(type = FieldType.Text, store = true)
    private String keyWords;

    @Field(type = FieldType.Text, store = true)
    private String paperAbstract;

    @Field(type = FieldType.Text, store = true)
    private String scienceField;

    @Field(type = FieldType.Text, store = true)
    private String text;
}
