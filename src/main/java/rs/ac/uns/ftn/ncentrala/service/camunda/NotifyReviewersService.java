package rs.ac.uns.ftn.ncentrala.service.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Account;
import rs.ac.uns.ftn.ncentrala.model.AccountAuthority;
import rs.ac.uns.ftn.ncentrala.model.Review;
import rs.ac.uns.ftn.ncentrala.model.SciencePaper;
import rs.ac.uns.ftn.ncentrala.service.AccountService;
import rs.ac.uns.ftn.ncentrala.service.EmailService;
import rs.ac.uns.ftn.ncentrala.service.ReviewService;
import rs.ac.uns.ftn.ncentrala.service.SciencePaperService;

import java.util.ArrayList;
import java.util.List;

@Service
public class NotifyReviewersService implements JavaDelegate {

    @Autowired
    EmailService emailService;

    @Autowired
    AccountService accountService;

    @Autowired
    SciencePaperService sciencePaperService;

    @Autowired
    ReviewService reviewService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        List<String> allAgain = (List<String>)delegateExecution.getVariable("allReviewers");


        String all = (String)delegateExecution.getVariable("all");
        List<String> reviewers = new ArrayList<>();
        String[] usernames = all.split(",");

        String paperTitle = (String)delegateExecution.getVariable("paperTitle");
        SciencePaper sciencePaper = this.sciencePaperService.getByTitle(paperTitle);
        List<Review> reviews = this.reviewService.findBySciencePaperId(sciencePaper.getId());
        if(allAgain == null)
            this.reviewService.deleteAll(reviews);

        for(String one: usernames){
            reviewers.add(one);
            Account account = this.accountService.findByUsername(one);
            this.emailService.notifyReviewers(account, sciencePaper);
        }

        if(allAgain != null){
            delegateExecution.setVariable("rewNum", reviewers.size() + allAgain.size());
        }else{
            delegateExecution.setVariable("rewNum", reviewers.size());
        }
        delegateExecution.setVariable("allReviewers", reviewers);
    }
}
