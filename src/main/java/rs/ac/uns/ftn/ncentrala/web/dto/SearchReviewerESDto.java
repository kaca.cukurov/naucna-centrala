package rs.ac.uns.ftn.ncentrala.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SearchReviewerESDto {

    private String firstName;
    private String lastName;
    private String email;
    private List<String> scienceFieldList;
    private String id;
}
