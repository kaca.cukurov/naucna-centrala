package rs.ac.uns.ftn.ncentrala.web.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.ncentrala.model.Journal;
import rs.ac.uns.ftn.ncentrala.model.SciencePaper;
import rs.ac.uns.ftn.ncentrala.model.SciencePaperES;
import rs.ac.uns.ftn.ncentrala.service.JournalService;
import rs.ac.uns.ftn.ncentrala.web.dto.JournalDto;
import rs.ac.uns.ftn.ncentrala.web.dto.ReviewerPaperDto;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/journal")
public class JournalController {

    @Autowired
    private JournalService journalService;

    @RequestMapping("/all")
    public ResponseEntity getAllSciencePapers() {
        List<Journal> journals = this.journalService.getAll();
        List<JournalDto> dtos = new ArrayList<>();
        for(Journal journal: journals)
            dtos.add(convertJournalToDto(journal));
        return new ResponseEntity<>(dtos, HttpStatus.OK);

    }

    private static JournalDto convertJournalToDto(Journal journal) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(journal, JournalDto.class);
    }
}
