package rs.ac.uns.ftn.ncentrala.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class QueryDto {

    private List<SimpleQueryDto> values = new ArrayList<>();
    private List<String> operations = new ArrayList();
    private boolean phrase;
}
