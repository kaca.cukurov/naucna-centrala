package rs.ac.uns.ftn.ncentrala.service.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.*;
import rs.ac.uns.ftn.ncentrala.model.enumeration.ScienceField;
import rs.ac.uns.ftn.ncentrala.service.*;

import java.util.ArrayList;
import java.util.List;

@Service
public class FindNewReviewerService implements JavaDelegate {
    @Autowired
    EmailService emailService;

    @Autowired
    AccountService accountService;

    @Autowired
    SciencePaperService sciencePaperService;

    @Autowired
    JournalService journalService;

    @Autowired
    ReviewService reviewService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        String title = (String)delegateExecution.getVariable("paperTitle");
        SciencePaper sciencePaper = this.sciencePaperService.getByTitle(title);
        List<Review> reviews = this.reviewService.findBySciencePaperId(sciencePaper.getId());
        List<String> all = (List<String>)delegateExecution.getVariable("allReviewers");
        List<String> yes = new ArrayList<>();
        String names = "";
        for(String user: all){
            Account account = this.accountService.findByUsername(user);
            boolean did = false;
            for(Review review: reviews){
                if(review.getReviewerId() == account.getId()){
                    did = true;
                }
            }
            if(!did) {
                names += " " + account.getFirstName() + " " + account.getLastName() + ",";
            }else
                yes.add(user);
        }
        names = names.substring(0, names.length()-1);
        this.emailService.reviewersFailed(this.accountService.findByUsername((String)delegateExecution.getVariable("fieldEditor")),
                sciencePaper, names);

        delegateExecution.setVariable("allReviewers", yes);
        delegateExecution.setVariable("doneReviewers", yes);
    }

}
