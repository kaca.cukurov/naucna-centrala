package rs.ac.uns.ftn.ncentrala.service;

import rs.ac.uns.ftn.ncentrala.model.Account;

import java.util.List;

public interface AccountService {

    Account save(Account account);

    Account findByUsername(String username);

    Account findById(Long id);
}
