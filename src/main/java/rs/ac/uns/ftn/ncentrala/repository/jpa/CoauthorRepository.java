package rs.ac.uns.ftn.ncentrala.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ncentrala.model.Coauthor;

import java.util.List;

public interface CoauthorRepository extends JpaRepository<Coauthor, Long> {

    List<Coauthor> findBySciencePaperId(Long id);
}
