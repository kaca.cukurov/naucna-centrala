package rs.ac.uns.ftn.ncentrala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Reviewer;
import rs.ac.uns.ftn.ncentrala.repository.jpa.ReviewerRepository;

import java.util.List;

@Service
public class ReviewerServiceImpl implements ReviewerService {

    @Autowired
    ReviewerRepository reviewerRepository;

    @Override
    public Reviewer save(Reviewer reviewer){
        return this.reviewerRepository.save(reviewer);
    }

    @Override
    public List<Reviewer> getAll(){
        return this.reviewerRepository.findAll();
    }

    @Override
    public Reviewer getOne(String id){
        return this.reviewerRepository.getOne(Long.parseLong(id));
    }
}
