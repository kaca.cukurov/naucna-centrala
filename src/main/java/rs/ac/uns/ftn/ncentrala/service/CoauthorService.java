package rs.ac.uns.ftn.ncentrala.service;

import rs.ac.uns.ftn.ncentrala.model.Coauthor;

import java.util.List;

public interface CoauthorService {

    Coauthor save(Coauthor coauthor);

    void deleteAll(Iterable<Coauthor> iterable);

    List<Coauthor> findBySciencePaperId(Long id);
}
