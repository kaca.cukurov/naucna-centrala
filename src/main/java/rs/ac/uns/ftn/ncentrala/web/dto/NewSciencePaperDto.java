package rs.ac.uns.ftn.ncentrala.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.uns.ftn.ncentrala.model.Coauthor;
import rs.ac.uns.ftn.ncentrala.model.SciencePaper;
import rs.ac.uns.ftn.ncentrala.model.enumeration.ScienceField;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NewSciencePaperDto {

    private String title;
    private List<CoauthorDto> coauthorDtos;
    private String keyWords;
    private String paperAbstract;
    private String scienceField;
    private String journalName;

    public NewSciencePaperDto(SciencePaper sciencePaper){
        this.title = sciencePaper.getTitle();
        this.keyWords = sciencePaper.getKeyWords();
        this.paperAbstract = sciencePaper.getPaperAbstract();
        this.scienceField = sciencePaper.getScienceField();
        this.journalName = sciencePaper.getJournalName();
        this.coauthorDtos = new ArrayList<>();
        for(Coauthor coauthor: sciencePaper.getCoauthors()){
            if(!coauthor.isMain()){
                CoauthorDto coauthorDto = new CoauthorDto();
                coauthorDto.setFirstName(coauthor.getFirstName());
                coauthorDto.setLastName(coauthor.getLastName());
                coauthorDto.setEmail(coauthor.getEmail());
                coauthorDto.setCity(coauthor.getCity());
                coauthorDto.setCountry(coauthor.getCountry());
                this.coauthorDtos.add(coauthorDto);
            }
        }
    }

}
