package rs.ac.uns.ftn.ncentrala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Journal;
import rs.ac.uns.ftn.ncentrala.repository.jpa.JournalRepository;

import java.util.List;

@Service
public class JournalServiceImpl implements JournalService {

    @Autowired
    JournalRepository journalRepository;

    @Override
    public Journal save(Journal journal){
        return this.journalRepository.save(journal);
    }

    @Override
    public List<Journal> getAll(){
        return this.journalRepository.findAll();
    }

    @Override
    public Journal findById(Long id){
        return this.journalRepository.getOne(id);
    }

    @Override
    public Journal findByTitle(String title){
        return this.journalRepository.findByTitle(title);
    }
}
