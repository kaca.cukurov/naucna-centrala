package rs.ac.uns.ftn.ncentrala.service.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Account;
import rs.ac.uns.ftn.ncentrala.model.SciencePaper;
import rs.ac.uns.ftn.ncentrala.service.AccountService;
import rs.ac.uns.ftn.ncentrala.service.EmailService;
import rs.ac.uns.ftn.ncentrala.service.SciencePaperService;

@Service
public class PaperAcceptedService implements JavaDelegate{

    @Autowired
    EmailService emailService;

    @Autowired
    SciencePaperService sciencePaperService;

    @Autowired
    AccountService accountService;


    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        String paperTitle = (String)delegateExecution.getVariable("paperTitle");
        SciencePaper sciencePaper = this.sciencePaperService.getByTitle(paperTitle);
        String author = (String)delegateExecution.getVariable("author");
        Account account = this.accountService.findByUsername(author);

        this.emailService.acceptPaper(account, sciencePaper);

    }
}
