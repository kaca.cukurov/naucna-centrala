package rs.ac.uns.ftn.ncentrala.web.controller;

import com.itextpdf.text.DocumentException;
import org.camunda.bpm.engine.rest.dto.VariableValueDto;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceDto;
import org.camunda.bpm.engine.rest.dto.task.TaskDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import rs.ac.uns.ftn.ncentrala.model.*;
import rs.ac.uns.ftn.ncentrala.search.QueryBuilder;
import rs.ac.uns.ftn.ncentrala.search.ResultRetrieverService;
import rs.ac.uns.ftn.ncentrala.security.JWTUtils;
import rs.ac.uns.ftn.ncentrala.service.*;
import rs.ac.uns.ftn.ncentrala.web.dto.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static rs.ac.uns.ftn.ncentrala.web.controller.UserController.JsonFromList;

@RestController
@RequestMapping(value = "/api/papers")
public class SciencePaperController {

    @Autowired
    StorageService storageService;

    @Autowired
    SciencePaperESService sciencePaperESService;

    @Autowired
    SciencePaperService sciencePaperService;

    @Autowired
    ResultRetrieverService resultRetrieverService;

    @Autowired
    CommentService commentService;

    @Autowired
    JWTUtils jwtUtils;

    @Autowired
    ReviewService reviewService;

    private final String path = System.getProperty("user.dir") + "\\src\\main\\resources\\upload-dir";
    private final String pathRest = "http://localhost:8084/rest/";


    @GetMapping(path = "/startPublishingProcess", produces = "application/json")
    public @ResponseBody FormFieldsDto startRegistration() {
        // start process
        String startProcessUrl = pathRest + "process-definition/key/"+ "Process_Main" + "/start";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity request = new HttpEntity<>("{}", headers);
        ResponseEntity<ProcessInstanceDto> result = restTemplate.postForEntity(startProcessUrl, request, ProcessInstanceDto.class);
        ProcessInstanceDto pi = result.getBody();

        // get first task
        String taskUrl = pathRest + "task?processInstanceId=" + pi.getId();
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        return new FormFieldsDto(task.getId(), pi.getId(), null);
    }

    @PostMapping(path = "/process/checkPayment/{processInstanceId}", produces = "application/json")
    public @ResponseBody ResponseEntity checkPayment(@RequestBody List<FormSubmissionDto> dto, @PathVariable String processInstanceId) {
        RestTemplate restTemplate = new RestTemplate();

        // get task
        String taskUrl = pathRest + "task?processInstanceId=" + processInstanceId;
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        // submit task form
        String submitUrl = pathRest + "/task/" + task.getId() + "/submit-form";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity request = new HttpEntity<>(JsonFromList(dto), headers);
        restTemplate.postForEntity(submitUrl, request, String.class);

        // get next user task
        resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        task = resultTask.getBody()[0];

        return new ResponseEntity<>(new FormFieldsDto(task.getName(), processInstanceId, null), HttpStatus.OK);
    }



    @PostMapping("/upload/{processInstanceId}")
    public ResponseEntity handleFileUpload(@RequestParam("file") MultipartFile file, @PathVariable String processInstanceId,
                                           @RequestParam("fileName") String fileName, @RequestParam("paperDto") String paperJson,
                                           @RequestHeader("Authentication-Token") String token) throws Exception{

        String username = this.jwtUtils.getUsernameFromToken(token);

        RestTemplate restTemplate = new RestTemplate();

        fileName = fileName.replace(' ', '_');
        String filePath = storageService.store(file, fileName);

        // get task
        String taskUrl = pathRest + "task?processInstanceId=" + processInstanceId;
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        List<FormSubmissionDto> dto = new ArrayList<>();
        filePath = filePath.replace("\\", "/");
        dto.add(new FormSubmissionDto("filePath", filePath));
        dto.add(new FormSubmissionDto("paperDto", paperJson));
        dto.add(new FormSubmissionDto("username", username));

        // submit task form
        String submitUrl = pathRest + "/task/" + task.getId() + "/submit-form";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity request = new HttpEntity<>(JsonFromPaperDto(dto), headers);
        System.out.println(JsonFromPaperDto(dto));
        restTemplate.postForEntity(submitUrl, request, String.class);

        Map<String, String> errorResponse = new HashMap<>();
        errorResponse.put("success", "You successfully uploaded " + file.getOriginalFilename() + "!");
        return new ResponseEntity<>(errorResponse, HttpStatus.OK);

    }

    @PostMapping("/uploadReply/{processInstanceId}")
    public ResponseEntity handleFileUpload(@RequestParam("file") MultipartFile file, @PathVariable String processInstanceId,
                                           @RequestParam("fileName") String fileName, @RequestParam("paperDto") String paperJson,
                                           @RequestParam("comment") String comment,
                                           @RequestHeader("Authentication-Token") String token) throws Exception{

        String username = this.jwtUtils.getUsernameFromToken(token);

        RestTemplate restTemplate = new RestTemplate();

        fileName = fileName.replace(' ', '_');
        String filePath = storageService.store(file, fileName);

        // get task
        String taskUrl = pathRest + "task?processInstanceId=" + processInstanceId;
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        List<FormSubmissionDto> dto = new ArrayList<>();
        filePath = filePath.replace("\\", "/");
        dto.add(new FormSubmissionDto("comment", comment));
        dto.add(new FormSubmissionDto("filePath", filePath));
        dto.add(new FormSubmissionDto("paperDto", paperJson));
        dto.add(new FormSubmissionDto("username", username));

        // submit task form
        String submitUrl = pathRest + "/task/" + task.getId() + "/submit-form";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity request = new HttpEntity<>(JsonFromPaperDto(dto), headers);
        System.out.println(JsonFromPaperDto(dto));
        restTemplate.postForEntity(submitUrl, request, String.class);

        Map<String, String> errorResponse = new HashMap<>();
        errorResponse.put("success", "You successfully uploaded " + file.getOriginalFilename() + "!");
        return new ResponseEntity<>(errorResponse, HttpStatus.OK);

    }

    @PostMapping(path = "/process/decline", produces = "application/json")
    public @ResponseBody ResponseEntity declinePaper(@RequestBody String processInstanceId) {
        RestTemplate restTemplate = new RestTemplate();

        // get task
        String taskUrl = pathRest + "task?processInstanceId=" + processInstanceId;
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        // complete task
        String completeUrl = pathRest + "/task/" + task.getId() + "/complete";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity request = new HttpEntity<>("{\"variables\":{\"relevant\": {\"value\":false}}}", httpHeaders);
        restTemplate.postForEntity(completeUrl, request, Object.class);

        // get next user task
        resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);

        if(resultTask.getBody().length == 0)
            return new ResponseEntity(HttpStatus.OK);
        else
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @PostMapping(path = "/process/accept", produces = "application/json")
    public @ResponseBody ResponseEntity acceptPaper(@RequestBody String processInstanceId) {
        RestTemplate restTemplate = new RestTemplate();

        // get task
        String taskUrl = pathRest + "task?processInstanceId=" + processInstanceId;
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        // complete task
        String completeUrl = pathRest + "/task/" + task.getId() + "/complete";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity request = new HttpEntity<>("{\"variables\":{\"relevant\": {\"value\":true}}}", httpHeaders);
        restTemplate.postForEntity(completeUrl, request, Object.class);

        // get paper id
        String getVarUrl = pathRest + "/process-instance/" + processInstanceId + "/variables/paperTitle";
        ResponseEntity responseEntity = restTemplate.getForEntity(getVarUrl, VariableValueDto.class);
        VariableValueDto valueDto = (VariableValueDto) responseEntity.getBody();
        SciencePaper sciencePaper = this.sciencePaperService.getByTitle((String)valueDto.getValue());

        return new ResponseEntity<>(sciencePaper.getId(), HttpStatus.OK);
    }

    @PostMapping(path = "/process/changeFormat/{processInstanceId}", produces = "application/json")
    public @ResponseBody ResponseEntity changeFormat(@RequestParam("comment") String comment, @RequestParam("sec") String sec,
                                                     @RequestParam("hours") String hours, @RequestParam("min") String min,
                                                     @RequestParam("days") String days, @RequestParam("months") String months,
                                                     @PathVariable String processInstanceId) {
        RestTemplate restTemplate = new RestTemplate();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.SECOND, Integer.parseInt(sec));
        c.add(Calendar.MINUTE, Integer.parseInt(min));
        c.add(Calendar.HOUR, Integer.parseInt(hours));
        c.add(Calendar.DATE, Integer.parseInt(days));
        c.add(Calendar.MONTH, Integer.parseInt(months));
        String output = sdf.format(c.getTime()) + "+01";

        // get task
        String taskUrl = pathRest + "task?processInstanceId=" + processInstanceId;
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        // complete task
        String completeUrl = pathRest + "/task/" + task.getId() + "/complete";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String body = "{\"variables\":{\"formatFine\": {\"value\":false}, \"comment\":{\"value\":\"" + comment + "\"}, " +
                "\"reFormat\":{\"value\":\"" + output + "\"}}}";
        HttpEntity request = new HttpEntity<>(body, httpHeaders);
        restTemplate.postForEntity(completeUrl, request, Object.class);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/process/acceptFormat", produces = "application/json")
    public @ResponseBody ResponseEntity acceptFormat(@RequestBody String processInstanceId) {
        RestTemplate restTemplate = new RestTemplate();

        // get task
        String taskUrl = pathRest + "task?processInstanceId=" + processInstanceId;
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        // complete task
        String completeUrl = pathRest + "/task/" + task.getId() + "/complete";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String body = "{\"variables\":{\"formatFine\": {\"value\":true}}}";
        HttpEntity request = new HttpEntity<>(body, httpHeaders);
        restTemplate.postForEntity(completeUrl, request, Object.class);

        return new ResponseEntity(HttpStatus.OK);
    }


    @GetMapping("/getCommentAndAuthors/{processInstanceId}")
    public ResponseEntity getComment(@PathVariable String processInstanceId) {
        RestTemplate restTemplate = new RestTemplate();
        AuthorsDto authorsDto = new AuthorsDto();
        // get authors' comment
        String getVarUrl = pathRest + "/process-instance/" + processInstanceId + "/variables/comment";
        ResponseEntity responseEntity = restTemplate.getForEntity(getVarUrl, VariableValueDto.class);
        VariableValueDto valueDto = (VariableValueDto) responseEntity.getBody();
        authorsDto.setComment((String)valueDto.getValue());
        // get title
        getVarUrl = pathRest + "/process-instance/" + processInstanceId + "/variables/paperTitle";
        responseEntity = restTemplate.getForEntity(getVarUrl, VariableValueDto.class);
        valueDto = (VariableValueDto) responseEntity.getBody();
        SciencePaper paper = this.sciencePaperService.getByTitle((String)valueDto.getValue());
        for(Coauthor coauthor : paper.getCoauthors()){
            authorsDto.getAuthors().add(new CoauthorDto(coauthor));
        }
        return new ResponseEntity<>(authorsDto, HttpStatus.OK);

    }

    @GetMapping("/coauthors/{processInstanceId}")
    public ResponseEntity getCoauthors(@PathVariable String processInstanceId) {
        RestTemplate restTemplate = new RestTemplate();
        List<CoauthorDto> coauthorDtos = new ArrayList<>();
        // get title
        String getVarUrl = pathRest + "/process-instance/" + processInstanceId + "/variables/paperTitle";
        ResponseEntity responseEntity = restTemplate.getForEntity(getVarUrl, VariableValueDto.class);
        VariableValueDto valueDto = (VariableValueDto) responseEntity.getBody();
        SciencePaper paper = this.sciencePaperService.getByTitle((String)valueDto.getValue());
        for(Coauthor coauthor : paper.getCoauthors()){
            coauthorDtos.add(new CoauthorDto(coauthor));
        }
        return new ResponseEntity<>(coauthorDtos, HttpStatus.OK);

    }

    @PostMapping("/finalDecision/{processInstanceId}")
    public ResponseEntity finalDecision(@PathVariable String processInstanceId,
                                        @RequestParam("paperState") String paperState, @RequestParam("sec") String sec,
                                        @RequestParam("hours") String hours, @RequestParam("min") String min,
                                        @RequestParam("days") String days, @RequestParam("months") String months) {
        RestTemplate restTemplate = new RestTemplate();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.SECOND, Integer.parseInt(sec));
        c.add(Calendar.MINUTE, Integer.parseInt(min));
        c.add(Calendar.HOUR, Integer.parseInt(hours));
        c.add(Calendar.DATE, Integer.parseInt(days));
        c.add(Calendar.MONTH, Integer.parseInt(months));
        String output = sdf.format(c.getTime()) + "+01";

        // get task
        String taskUrl = pathRest + "task?processInstanceId=" + processInstanceId;
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        // complete task
        String completeUrl = pathRest + "/task/" + task.getId() + "/complete";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String body = "{\"variables\":{\"paperState\": {\"value\":\"" + paperState + "\"},\"timeLimit\":{\"value\":\"" + output + "\"}}}";
        HttpEntity request = new HttpEntity<>(body, httpHeaders);
        restTemplate.postForEntity(completeUrl, request, Object.class);

        return new ResponseEntity(HttpStatus.OK);

    }

    @RequestMapping("/all")
    public ResponseEntity getAllSciencePapers() {
        List<SciencePaperES> papers = this.sciencePaperESService.findAll();
        List<ReviewerPaperDto> titles = new ArrayList<>();
        for(SciencePaperES paperES : papers)
            titles.add(new ReviewerPaperDto(paperES.getId(), paperES.getTitle()));
        return new ResponseEntity<>(titles, HttpStatus.OK);

    }

    @RequestMapping("/byTitle/{title}")
    public ResponseEntity getByTitle(@PathVariable String title) {
        SciencePaper sciencePaper = this.sciencePaperService.getByTitle(title);
        SciencePaperDto paperDto = new SciencePaperDto(sciencePaper);
        return new ResponseEntity<>(paperDto, HttpStatus.OK);
    }

    @RequestMapping("/reviewCommentsByTitle/{title}")
    public ResponseEntity reviewCommentsByTitle(@PathVariable String title) {
        SciencePaper sciencePaper = this.sciencePaperService.getByTitle(title);
        NewSciencePaperDto paperDto = new NewSciencePaperDto(sciencePaper);
        ReviewCommentsDto commentDto = new ReviewCommentsDto();
        commentDto.setSciencePaperDto(paperDto);
        List<String> comments = new ArrayList<>();
        List<Review> reviews = this.reviewService.findBySciencePaperId(sciencePaper.getId());
        for(Review r: reviews)
            comments.add(r.getCommentAuthor());
        commentDto.setReviews(comments);
        return new ResponseEntity<>(commentDto, HttpStatus.OK);
    }

    @RequestMapping("/commentByTitle/{title}")
    public ResponseEntity commentByTitle(@PathVariable String title) {
        SciencePaper sciencePaper = this.sciencePaperService.getByTitle(title);
        NewSciencePaperDto paperDto = new NewSciencePaperDto(sciencePaper);
        Comment comment = this.commentService.findBySciencePaperId(sciencePaper.getId());
        CommentDto commentDto = new CommentDto(comment.getComment(), paperDto);
        return new ResponseEntity<>(commentDto, HttpStatus.OK);
    }

    @PostMapping(value="/search", consumes="application/json")
    public ResponseEntity<List<SearchSciencePaperDto>> searchTermQuery(@RequestBody QueryDto simpleQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(simpleQuery.getValues(),
                simpleQuery.getOperations(), simpleQuery.isPhrase());

        List<SearchSciencePaperDto> results = resultRetrieverService.getResults(query);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @RequestMapping(value="pdf/{title}", method=RequestMethod.GET)
    public ResponseEntity<byte[]> downloadPdf(@PathVariable String title) throws IOException, DocumentException {

        SciencePaper paper = this.sciencePaperService.getByTitle(title);
        String filename = paper.getTitle().replace(' ', '_') + ".pdf";

        File pdfFile = Paths.get(path + "/" + filename).toFile();

        byte[] contents = Files.readAllBytes(pdfFile.toPath());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        // Here you have to set the actual filename of your pdf
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
        return response;

    }

    public static String JsonFromPaperDto(List<FormSubmissionDto> list){
        String json = "{ \"variables\" : {";
        for(FormSubmissionDto dto : list){
            if(dto.getField().equals("paperDto")){
                json += "\"" + dto.getField() + "\": { \"value\":" + dto.getValue() + "},";
            }else
                json += "\"" + dto.getField() + "\": { \"value\":\"" + dto.getValue() + "\"},";
        }
        json = json.substring(0, json.length()-2);
        json += "}}}";
        return json;
    }

}
