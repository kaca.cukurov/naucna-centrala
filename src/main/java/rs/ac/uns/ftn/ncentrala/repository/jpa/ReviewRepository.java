package rs.ac.uns.ftn.ncentrala.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ncentrala.model.Review;

import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Long> {

    List<Review> findBySciencePaperId(Long id);

    List<Review> findByReviewerId(Long id);

}
