package rs.ac.uns.ftn.ncentrala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.AccountAuthority;
import rs.ac.uns.ftn.ncentrala.repository.jpa.AccountAuthorityRepository;

@Service
public class AccountAuthorityServiceImpl implements AccountAuthorityService {

    @Autowired
    AccountAuthorityRepository accountAuthorityRepository;

    @Override
    public AccountAuthority save(AccountAuthority accountAuthority){
        return this.accountAuthorityRepository.save(accountAuthority);
    }
}
