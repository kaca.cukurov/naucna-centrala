package rs.ac.uns.ftn.ncentrala.service.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Account;
import rs.ac.uns.ftn.ncentrala.model.SciencePaper;
import rs.ac.uns.ftn.ncentrala.service.AccountService;
import rs.ac.uns.ftn.ncentrala.service.EmailService;
import rs.ac.uns.ftn.ncentrala.service.SciencePaperService;

@Service
public class DeclinePaperTimeService implements JavaDelegate {

    @Autowired
    EmailService emailService;

    @Autowired
    AccountService accountService;

    @Autowired
    SciencePaperService sciencePaperService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        String username = (String)delegateExecution.getVariable("author");
        String title =(String)delegateExecution.getVariable("paperTitle");

        Account account = this.accountService.findByUsername(username);
        SciencePaper sciencePaper = this.sciencePaperService.getByTitle(title);

        this.emailService.declinePaperATime(account, sciencePaper);



    }
}
