package rs.ac.uns.ftn.ncentrala.service;

import com.sun.mail.imap.protocol.ID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.ReviewerES;
import rs.ac.uns.ftn.ncentrala.repository.elasticsearch.ReviewerESRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReviewerESService {

    @Autowired
    ReviewerESRepository reviewerESRepository;

    public ReviewerES save(ReviewerES reviewerES){
        return this.reviewerESRepository.save(reviewerES);
    }

    public List<ReviewerES> findAll(){
        List<ReviewerES> target = new ArrayList<>();
        this.reviewerESRepository.findAll().forEach(target::add);
        return  target;
    }

    public ReviewerES findOne(String id){
        System.out.println(id);
        Optional<ReviewerES> reviewerES = this.reviewerESRepository.findById(id);
        return reviewerES.get();
    }

    public List<ReviewerES> findByScienceFields(String field){
        System.out.println(field);
        return this.reviewerESRepository.findScienceField(field);
    }

    public ReviewerES findById(ID id){
        return this.reviewerESRepository.findById(id);
    }
}
