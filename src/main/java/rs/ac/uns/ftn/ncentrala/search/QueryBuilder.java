package rs.ac.uns.ftn.ncentrala.search;

import lombok.Getter;
import lombok.Setter;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.*;
import rs.ac.uns.ftn.ncentrala.web.dto.SimpleQueryDto;

import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.matchPhraseQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

@Getter
@Setter
public class QueryBuilder {

    private static int maxEdits = 1;

    public static org.elasticsearch.index.query.QueryBuilder buildQuery(List<SimpleQueryDto> fields, List<String> operations,
                                                                        boolean phrase) throws IllegalArgumentException, ParseException{
        String errorMessage = "";
        List<org.elasticsearch.index.query.QueryBuilder> allQueries = new ArrayList<>();
        for (SimpleQueryDto entry : fields)
        {
            String field = entry.getField();
            String value = entry.getValue();

            if(field == null || field.equals("")){
                errorMessage += "Field not specified";
            }
            if(value == null){
                if(!errorMessage.equals("")) errorMessage += "\n";
                errorMessage += "Value not specified";
            }
            if(!errorMessage.equals("")){
                throw new IllegalArgumentException(errorMessage);
            }

            org.elasticsearch.index.query.QueryBuilder retVal = null;

            if(phrase){
                if(field.equals("coauthors")){
                    retVal = new NestedQueryBuilder("coauthors",
                            QueryBuilders.boolQuery().should(matchPhraseQuery("coauthors.firstName", value))
                                    .should(matchPhraseQuery("coauthors.lastName", value)), ScoreMode.Avg);
                }else{
                    retVal = new MatchPhraseQueryBuilder(field, value);
                }
            }else{
                if(field.equals("coauthors")){
                    retVal = new NestedQueryBuilder("coauthors",
                            QueryBuilders.boolQuery().should(matchQuery("coauthors.firstName", value))
                                    .should(matchQuery("coauthors.lastName", value)), ScoreMode.Avg);
                }else{
                    retVal = new MatchQueryBuilder(field, value);
                }
            }

            allQueries.add(retVal);
        }

        BoolQueryBuilder finalQuery = QueryBuilders.boolQuery();

        if(!operations.isEmpty()){
            int counter = 0;
            boolean firstTime = true;
            for(org.elasticsearch.index.query.QueryBuilder query: allQueries) {
                String oper = operations.get(counter);
                if (oper.equals("AND"))
                    finalQuery.must(query);
                else if (oper.equals("OR"))
                    finalQuery.should(query);
                if (!firstTime)
                    counter++;
                firstTime = false;
            }
        }else{
            for(org.elasticsearch.index.query.QueryBuilder query: allQueries){
                finalQuery.must(query);
            }
        }

        return finalQuery;
    }


}
