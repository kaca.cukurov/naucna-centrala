package rs.ac.uns.ftn.ncentrala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.SciencePaperES;
import rs.ac.uns.ftn.ncentrala.repository.elasticsearch.SciencePaperESRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SciencePaperESService {

    @Autowired
    SciencePaperESRepository sciencePaperESRepository;

    public SciencePaperES save(SciencePaperES paperES){
        return this.sciencePaperESRepository.save(paperES);
    }

    public List<SciencePaperES> findAll(){
        List<SciencePaperES> target = new ArrayList<>();
        this.sciencePaperESRepository.findAll().forEach(target::add);
        return  target;
    }

    public SciencePaperES findOne(String id){
        Optional<SciencePaperES> sciencePaperES = this.sciencePaperESRepository.findById(id);
        System.out.println(sciencePaperES);
        System.out.println(sciencePaperES.get());
        return sciencePaperES.get();
    }

}
