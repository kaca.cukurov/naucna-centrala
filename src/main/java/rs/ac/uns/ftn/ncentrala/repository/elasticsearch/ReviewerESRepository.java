package rs.ac.uns.ftn.ncentrala.repository.elasticsearch;

import com.sun.mail.imap.protocol.ID;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.ncentrala.model.ReviewerES;

import java.util.List;

@Repository
public interface ReviewerESRepository extends ElasticsearchRepository<ReviewerES, String> {

    ReviewerES findById(ID id);

    @Query("{\"match_phrase\": {\"scienceFieldList\": \"?0\"}}")
    List<ReviewerES> findScienceField(String scienceFields);
}
