package rs.ac.uns.ftn.ncentrala.service;

import rs.ac.uns.ftn.ncentrala.model.Comment;

public interface CommentService {

    Comment save(Comment comment);

    Comment findOne(Long id);

    Comment findBySciencePaperId(Long id);
}
