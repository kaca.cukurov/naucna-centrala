package rs.ac.uns.ftn.ncentrala.service;

import rs.ac.uns.ftn.ncentrala.model.SciencePaper;

import java.util.List;

public interface SciencePaperService {

    SciencePaper save(SciencePaper paper);

    List<SciencePaper> getAll();

    SciencePaper getOne(String id);

    SciencePaper getByTitle(String title);
}
