package rs.ac.uns.ftn.ncentrala.web.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReviewDto {

    private String paperTitle;
    private String commentAuthor;
    private String commentEditor;
    private String recommendation;
}
