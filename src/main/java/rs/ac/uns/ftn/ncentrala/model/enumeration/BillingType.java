package rs.ac.uns.ftn.ncentrala.model.enumeration;

public enum BillingType {
    AUTHORS, READERS
}
