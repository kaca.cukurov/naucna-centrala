package rs.ac.uns.ftn.ncentrala.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.*;

@Service
public class StorageService {

    Logger log = LoggerFactory.getLogger(this.getClass().getName());

    private final String path = System.getProperty("user.dir") + "\\src\\main\\resources\\upload-dir";
    private final Path rootLocation = Paths.get(this.path);

    public String store(MultipartFile file, String fileName) {
        try {
            Files.deleteIfExists(this.rootLocation.resolve(fileName + ".pdf"));
            Files.copy(file.getInputStream(), this.rootLocation.resolve(fileName + ".pdf"));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("FAIL!");
        }
        return path + "\\" +  fileName + ".pdf";
    }

}
