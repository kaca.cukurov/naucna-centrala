package rs.ac.uns.ftn.ncentrala.web.controller;


import org.aspectj.weaver.ast.Var;
import org.camunda.bpm.engine.rest.dto.VariableValueDto;
import org.camunda.bpm.engine.rest.dto.task.TaskDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import rs.ac.uns.ftn.ncentrala.security.JWTUtils;
import rs.ac.uns.ftn.ncentrala.web.dto.MyTaskDto;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/api/editor")
public class EditorController {

    @Autowired
    JWTUtils jwtUtils;

    private final String pathRest = "http://localhost:8084/rest/";

    @GetMapping("/tasks")
    public ResponseEntity handleFileUpload(@RequestHeader("Authentication-Token") String token) throws Exception{

        String username = this.jwtUtils.getUsernameFromToken(token);
        RestTemplate restTemplate = new RestTemplate();

        // get all tasks for assignee by username
        String getTaskUrl = pathRest + "/task";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String body = "{\"assignee\":\"" + username + "\"}";
        HttpEntity request = new HttpEntity<>(body, headers);
        ResponseEntity response = restTemplate.postForEntity(getTaskUrl, request, Object.class);
        List<LinkedHashMap<String, Object>> taskDtoList = (List<LinkedHashMap<String, Object>>)response.getBody();

        // get paper title for each process
        String getVarUrl = pathRest + "/process-instance/";//{id}/variables/{varName}
        List<MyTaskDto> myTaskDtos = new ArrayList<>();
        for(LinkedHashMap<String, Object> dto: taskDtoList){
            String url = getVarUrl + dto.get("processInstanceId") + "/variables/paperTitle";
            ResponseEntity responseEntity = restTemplate.getForEntity(url, VariableValueDto.class);
            VariableValueDto valueDto = (VariableValueDto) responseEntity.getBody();
            myTaskDtos.add(new MyTaskDto((String)dto.get("id"), (String)dto.get("processInstanceId"), (String)dto.get("name"), (String)valueDto.getValue()));
        }

        return new ResponseEntity<>(myTaskDtos, HttpStatus.OK);

    }
}
