package rs.ac.uns.ftn.ncentrala.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ncentrala.model.ScientificCommittee;

public interface ScientificCommitteeRepository extends JpaRepository<ScientificCommittee, Long> {
}
