package rs.ac.uns.ftn.ncentrala.web.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.ncentrala.web.dto.PathDto;

@RestController
@RequestMapping(value = "/api/sep")
public class SepController {

    @Value("${sepfront.path}")
    private String sepFrontPath;

    @Value("${naucnaCentralafront.path}")
    private String naucnaCentralaFrontPath;

    @Value("${customer_id}")
    private String customerId;

    @RequestMapping("/getUrl")
    public ResponseEntity getSepFrontUrl() {
        PathDto pathDto = new PathDto(sepFrontPath);
        return new ResponseEntity<>(pathDto, HttpStatus.OK);
    }

    @RequestMapping("/getFrontUrl")
    public ResponseEntity getNaucnaCentralaFrontUrl() {
        PathDto pathDto = new PathDto(naucnaCentralaFrontPath);
        return new ResponseEntity<>(pathDto, HttpStatus.OK);
    }

    @RequestMapping("/getCustomerId")
    public ResponseEntity getCustomerId() {
        PathDto pathDto = new PathDto(customerId);
        return new ResponseEntity<>(pathDto, HttpStatus.OK);
    }

}
