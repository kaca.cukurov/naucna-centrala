package rs.ac.uns.ftn.ncentrala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.SciencePaper;
import rs.ac.uns.ftn.ncentrala.repository.jpa.SciencePaperRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class SciencePaperServiceImpl implements SciencePaperService {

    @Autowired
    SciencePaperRepository sciencePaperRepository;

    @Override
    public SciencePaper save(SciencePaper paper){
        return this.sciencePaperRepository.save(paper);
    }

    @Override
    public List<SciencePaper> getAll(){
        return this.sciencePaperRepository.findAll();
    }

    @Override
    public SciencePaper getOne(String id){
        return this.sciencePaperRepository.getOne(Long.parseLong(id));
    }

    @Override
    public SciencePaper getByTitle(String title){
        return this.sciencePaperRepository.getByTitle(title);
    }
}
