package rs.ac.uns.ftn.ncentrala.model.enumeration;

public enum ScienceField {
    MECHANICAL_ENGINEERING, COMPUTER_SCIENCE, ROBOTICS_AND_AUTOMATION, GENERAL_MATHEMATICS, STATISTICS
}
