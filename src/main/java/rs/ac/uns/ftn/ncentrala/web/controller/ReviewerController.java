package rs.ac.uns.ftn.ncentrala.web.controller;

import org.camunda.bpm.engine.rest.dto.VariableValueDto;
import org.camunda.bpm.engine.rest.dto.task.TaskDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import rs.ac.uns.ftn.ncentrala.model.*;
import rs.ac.uns.ftn.ncentrala.model.enumeration.ScienceField;
import rs.ac.uns.ftn.ncentrala.search.ReviewerSearch;
import rs.ac.uns.ftn.ncentrala.security.JWTUtils;
import rs.ac.uns.ftn.ncentrala.service.*;
import rs.ac.uns.ftn.ncentrala.web.dto.*;

import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping(value = "/api/reviewers")
public class ReviewerController {

    @Autowired
    ReviewerESService reviewerESService;

    @Autowired
    ReviewerService reviewerService;

    @Autowired
    SciencePaperESService sciencePaperESService;

    @Autowired
    SciencePaperService sciencePaperService;

    @Autowired
    ReviewerSearch reviewerSearch;

    @Autowired
    JournalService journalService;

    @Autowired
    AccountService accountService;

    @Autowired
    ReviewService reviewService;

    @Autowired
    JWTUtils jwtUtils;

    private final String pathRest = "http://localhost:8084/rest/";

    @PostMapping(path = "/process/chiefEditor", produces = "application/json")
    public @ResponseBody ResponseEntity setChiefEditorToChooseReviewers(@RequestBody String processInstanceId) {
        RestTemplate restTemplate = new RestTemplate();

        // get task
        String taskUrl = pathRest + "task?processInstanceId=" + processInstanceId;
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        // complete task
        String completeUrl = pathRest + "/task/" + task.getId() + "/complete";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String body = "{\"variables\":{\"noReviewers\": {\"value\":true}}}";
        HttpEntity request = new HttpEntity<>(body, httpHeaders);
        restTemplate.postForEntity(completeUrl, request, Object.class);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/process/setReviewers/{processInstanceId}", produces = "application/json")
    public @ResponseBody ResponseEntity setReviewersToReview(@PathVariable String processInstanceId,
                                                             @RequestParam("usernames") String usernames, @RequestParam("sec") String sec,
                                                             @RequestParam("hours") String hours, @RequestParam("min") String min,
                                                             @RequestParam("days") String days, @RequestParam("months") String months) {
        RestTemplate restTemplate = new RestTemplate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.SECOND, Integer.parseInt(sec));
        c.add(Calendar.MINUTE, Integer.parseInt(min));
        c.add(Calendar.HOUR, Integer.parseInt(hours));
        c.add(Calendar.DATE, Integer.parseInt(days));
        c.add(Calendar.MONTH, Integer.parseInt(months));
        String output = sdf.format(c.getTime()) + "+01";

        // get task
        String taskUrl = pathRest + "task?processInstanceId=" + processInstanceId;
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        String valueDto = "";
        String[] luser = usernames.split(",");
        for(String username: luser)
            valueDto += username + ",";
        valueDto = valueDto.substring(0, valueDto.length()-1);

        // complete task
        String completeUrl = pathRest + "/task/" + task.getId() + "/complete";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String body = "{\"variables\":{\"all\": {\"value\":\"" + valueDto + "\"},\"noReviewers\": {\"value\":false}," +
                " \"reviewTime\":{\"value\":\"" + output + "\"}}}";
        HttpEntity request = new HttpEntity<>(body, httpHeaders);
        restTemplate.postForEntity(completeUrl, request, Object.class);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/process/saveReview/{processInstanceId}", produces = "application/json")
    public @ResponseBody ResponseEntity saveReview(@PathVariable String processInstanceId, @RequestBody ReviewDto reviewDto,
                                                   @RequestHeader("Authentication-Token") String token) {
        RestTemplate restTemplate = new RestTemplate();
        String username = this.jwtUtils.getUsernameFromToken(token);

        // get task
        String getTaskUrl = pathRest + "/task";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String body = "{\"assignee\":\"" + username + "\", \"name\":\"Recenziranje\", \"processInstanceId\":\""+processInstanceId + "\"}";
        HttpEntity request = new HttpEntity<>(body, headers);
        ResponseEntity<TaskDto[]> response = restTemplate.postForEntity(getTaskUrl, request, TaskDto[].class);
        TaskDto task = response.getBody()[0];

        Review review = convertDtoToReview(reviewDto);
        review.setReviewerId(this.accountService.findByUsername(username).getId());
        review.setSciencePaperId(this.sciencePaperService.getByTitle(reviewDto.getPaperTitle()).getId());
        this.reviewService.save(review);

        // complete task
        String completeUrl = pathRest + "/task/" + task.getId() + "/complete";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        request = new HttpEntity<>("{\"variables\":{}}", httpHeaders);
        restTemplate.postForEntity(completeUrl, request, Object.class);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/process/decide/{processInstanceId}", produces = "application/json")
    public @ResponseBody ResponseEntity decide(@PathVariable String processInstanceId,
                                                             @RequestParam("decision") String decision, @RequestParam("sec") String sec,
                                                             @RequestParam("hours") String hours, @RequestParam("min") String min,
                                                             @RequestParam("days") String days, @RequestParam("months") String months) {
        RestTemplate restTemplate = new RestTemplate();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.SECOND, Integer.parseInt(sec));
        c.add(Calendar.MINUTE, Integer.parseInt(min));
        c.add(Calendar.HOUR, Integer.parseInt(hours));
        c.add(Calendar.DATE, Integer.parseInt(days));
        c.add(Calendar.MONTH, Integer.parseInt(months));
        String output = sdf.format(c.getTime()) + "+01";

        // get task
        String taskUrl = pathRest + "task?processInstanceId=" + processInstanceId;
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        // complete task
        String completeUrl = pathRest + "/task/" + task.getId() + "/complete";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String body = "{\"variables\":{\"decision\": {\"value\":\"" + decision + "\"}, \"timeLimit\":{\"value\":\"" + output + "\"}}}";
        HttpEntity request = new HttpEntity<>(body, httpHeaders);
        restTemplate.postForEntity(completeUrl, request, Object.class);

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @GetMapping(path = "/recoms/{title}", produces = "application/json")
    public @ResponseBody ResponseEntity getRecoms(@PathVariable String title) {

        SciencePaper sciencePaper = this.sciencePaperService.getByTitle(title);
        List<Review> reviews = this.reviewService.findBySciencePaperId(sciencePaper.getId());
        List<RecomDto> recomDtos = new ArrayList<>();
        for(Review review: reviews){
            RecomDto recomDto = new RecomDto();
            recomDto.setCommentEditor(review.getCommentEditor());
            recomDto.setRecommendation(review.getRecommendation());
            Account account = this.accountService.findById(review.getReviewerId());
            recomDto.setReviewer(account.getFirstName() + " " + account.getLastName());
            recomDtos.add(recomDto);
        }
        return new ResponseEntity<>(recomDtos, HttpStatus.OK);
    }


    @PostMapping("/save")
    public ResponseEntity<ReviewerES> save(@RequestBody NewReviewerESDto reviewerESDto) throws Exception {

        ReviewerES reviewerES = convertNewReviewerESDtotoReviewerES(reviewerESDto);
        reviewerES = this.reviewerESService.save(reviewerES);
        return ResponseEntity.status(HttpStatus.CREATED).body(reviewerES);
    }

    @PostMapping("/upload")
    public ResponseEntity<NewReviewerESDto> saveNewReviewer(@RequestBody NewReviewerESDto reviewerESDto) throws Exception{

        List<SciencePaperES> paperESList = new ArrayList<>();
        List<SciencePaper> papers = new ArrayList<>();
        for(String id: reviewerESDto.getPaperIds()) {
            paperESList.add(this.sciencePaperESService.findOne(id));
            papers.add(this.sciencePaperService.getOne(id));
        }

        ReviewerES reviewerES = convertNewReviewerESDtotoReviewerES(reviewerESDto);
        Reviewer reviewer = convertNewReviewerESDtotoReviewer(reviewerESDto);
        reviewerES.setPapers(paperESList);
        reviewer = this.reviewerService.save(reviewer);
        reviewerES.setId(reviewer.getId().toString());
        this.reviewerESService.save(reviewerES);


        return ResponseEntity.status(HttpStatus.CREATED).body(reviewerESDto);
    }

    @GetMapping("/all/es")
    public ResponseEntity getAllReviewersES() {
        return new ResponseEntity<>(this.reviewerESService.findAll(), HttpStatus.OK);

    }


    @PostMapping("/all/{processInstanceId}")
    public ResponseEntity getAllReviewers(@PathVariable String processInstanceId, @RequestParam String title) {

        SciencePaper paper = this.sciencePaperService.getByTitle(title);
        Journal journal = this.journalService.findByTitle(paper.getJournalName());
        List<Account> reviewers = journal.getReviewers();
        List<ReviewerDto> dtos = new ArrayList<>();
        for(Account account: reviewers)
            dtos.add(new ReviewerDto(account));
        return new ResponseEntity<>(filter(dtos, paper), HttpStatus.OK);

    }

    @PostMapping(value="/search")
    public ResponseEntity moreLikeThis(@RequestParam("title") String title) throws Exception {
        List<SearchReviewerESDto> reviewerESs = reviewerSearch.moreLikeThisSearch(title);
        SciencePaper paper = this.sciencePaperService.getByTitle(title);
        List<ReviewerDto> reviewerDtos = new ArrayList<>();
        for(SearchReviewerESDto dto: reviewerESs){
            reviewerDtos.add(new ReviewerDto(this.accountService.findById(Long.parseLong(dto.getId()))));
        }
        return new ResponseEntity<>(filter(reviewerDtos, paper), HttpStatus.OK);
    }

    @PostMapping(value = "/geo")
    public ResponseEntity geoSearch(@RequestParam("title") String title){
        List<SearchReviewerESDto> reviewerESs = reviewerSearch.geoSearch(title);
        SciencePaper paper = this.sciencePaperService.getByTitle(title);
        List<ReviewerDto> reviewerDtos = new ArrayList<>();
        for(SearchReviewerESDto dto: reviewerESs){
            reviewerDtos.add(new ReviewerDto(this.accountService.findById(Long.parseLong(dto.getId()))));
        }
        return new ResponseEntity<>(filter(reviewerDtos, paper), HttpStatus.OK);

    }

    @PostMapping(value = "/field")
    public ResponseEntity fieldsSearch(@RequestParam("title") String title){
        SciencePaper sciencePaper = this.sciencePaperService.getByTitle(title);
        List<ReviewerES> reviewerESList = this.reviewerESService.findByScienceFields(sciencePaper.getScienceField());
        List<ReviewerDto> reviewerDtos = new ArrayList<>();
        for(ReviewerES reviewer: reviewerESList)
            reviewerDtos.add(new ReviewerDto(this.accountService.findById(Long.parseLong(reviewer.getId()))));

        return new ResponseEntity<>(filter(reviewerDtos, sciencePaper), HttpStatus.OK);
    }


    private static ReviewerES convertNewReviewerESDtotoReviewerES(NewReviewerESDto reviewerESDto) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(reviewerESDto, ReviewerES.class);
    }

    public static Reviewer convertNewReviewerESDtotoReviewer(NewReviewerESDto reviewerESDto) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(reviewerESDto, Reviewer.class);
    }

    public static SearchReviewerESDto convertReviewerToSearch(ReviewerES reviewerES) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(reviewerES, SearchReviewerESDto.class);
    }

    public static Review convertDtoToReview(ReviewDto reviewDto){
        ModelMapper mapper = new ModelMapper();
        return mapper.map(reviewDto, Review.class);
    }

    private List<ReviewerDto> filter(List<ReviewerDto> reviewers, SciencePaper paper){
        List<ReviewerDto> dtos = new ArrayList<>();
        List<Review> reviews = this.reviewService.findBySciencePaperId(paper.getId());
        for(ReviewerDto reviewerDto: reviewers) {
            boolean found = false;
            if(reviews != null){
                for(Review review: reviews){
                    if(review.getReviewerId() == reviewerDto.getId())
                        found = true;
                }
            }
            if(!found)
                dtos.add(reviewerDto);
        }
        return dtos;
    }
}
