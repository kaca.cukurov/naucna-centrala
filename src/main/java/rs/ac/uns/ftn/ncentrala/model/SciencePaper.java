package rs.ac.uns.ftn.ncentrala.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import org.hibernate.cfg.IndexOrUniqueKeySecondPass;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import rs.ac.uns.ftn.ncentrala.model.enumeration.ScienceField;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class SciencePaper {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "paper_id")
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String keyWords;

    @Column(nullable = false)
    private String paperAbstract;

    @Field(type = FieldType.Text, store = true)
    private String scienceField;

    @Column(nullable = false)
    private String journalName;

    @OneToMany(mappedBy = "sciencePaper", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Coauthor> coauthors;

    @ManyToMany(mappedBy = "sciencePapers")
    private List<Account> reviewers;

    @Column(nullable = false)
    private String filePath;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;
}
