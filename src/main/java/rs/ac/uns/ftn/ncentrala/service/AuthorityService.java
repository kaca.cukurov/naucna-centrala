package rs.ac.uns.ftn.ncentrala.service;

import rs.ac.uns.ftn.ncentrala.model.Authority;

public interface AuthorityService {

    Authority save(Authority authority);

    Authority findByType(String string);
}
