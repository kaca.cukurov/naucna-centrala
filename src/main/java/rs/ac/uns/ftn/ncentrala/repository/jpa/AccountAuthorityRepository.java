package rs.ac.uns.ftn.ncentrala.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ncentrala.model.AccountAuthority;

public interface AccountAuthorityRepository extends JpaRepository<AccountAuthority, Long> {
}
