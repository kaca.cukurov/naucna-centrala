package rs.ac.uns.ftn.ncentrala.service.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Account;
import rs.ac.uns.ftn.ncentrala.model.Comment;
import rs.ac.uns.ftn.ncentrala.model.SciencePaper;
import rs.ac.uns.ftn.ncentrala.service.*;

@Service
public class ChangeFormatService implements JavaDelegate{

    @Autowired
    EmailService emailService;

    @Autowired
    CommentService commentService;

    @Autowired
    AccountService accountService;

    @Autowired
    SciencePaperService sciencePaperService;

    @Autowired
    JournalService journalService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        String comment = (String)delegateExecution.getVariable("comment");
        String username = (String)delegateExecution.getVariable("author");
        String title =(String)delegateExecution.getVariable("paperTitle");

        Account account = this.accountService.findByUsername(username);
        SciencePaper sciencePaper = this.sciencePaperService.getByTitle(title);

        Comment comment1 = new Comment();
        comment1.setComment(comment);
        comment1.setSciencePaperId(sciencePaper.getId());
        this.commentService.save(comment1);

        this.emailService.changeFormat(account, sciencePaper, comment);

    }
}
