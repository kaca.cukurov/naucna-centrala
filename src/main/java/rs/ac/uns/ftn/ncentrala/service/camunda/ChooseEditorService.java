package rs.ac.uns.ftn.ncentrala.service.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Account;
import rs.ac.uns.ftn.ncentrala.model.Journal;
import rs.ac.uns.ftn.ncentrala.model.SciencePaper;
import rs.ac.uns.ftn.ncentrala.model.ScientificCommittee;
import rs.ac.uns.ftn.ncentrala.model.enumeration.ScienceField;
import rs.ac.uns.ftn.ncentrala.service.AccountService;
import rs.ac.uns.ftn.ncentrala.service.EmailService;
import rs.ac.uns.ftn.ncentrala.service.JournalService;
import rs.ac.uns.ftn.ncentrala.service.SciencePaperService;

@Service
public class ChooseEditorService implements JavaDelegate {

    @Autowired
    EmailService emailService;

    @Autowired
    AccountService accountService;

    @Autowired
    SciencePaperService sciencePaperService;

    @Autowired
    JournalService journalService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        String paperTitle = (String)delegateExecution.getVariable("paperTitle");
        SciencePaper sciencePaper = this.sciencePaperService.getByTitle(paperTitle);
        Journal journal = this.journalService.findByTitle(sciencePaper.getJournalName());
        ScientificCommittee committee = journal.getScientificCommittee();
        Account chosenEditor = null;
        System.out.println(journal.getTitle());
        System.out.println(journal.getScientificCommittee().getFieldEditors().size());
        for(Account account: committee.getFieldEditors()){
            System.out.println(account);
            for(ScienceField field: account.getScienceFieldList()){
                System.out.println(field.toString());
                if(field.toString().equals(sciencePaper.getScienceField()))
                    chosenEditor = account;
            }
        }
        if(chosenEditor == null)
            chosenEditor = committee.getChiefEditor();

        delegateExecution.setVariable("fieldEditor", chosenEditor.getUsername());
        this.emailService.editorToChooseReviewers(chosenEditor, sciencePaper);

    }
}
