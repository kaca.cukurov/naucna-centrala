package rs.ac.uns.ftn.ncentrala.service.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Journal;
import rs.ac.uns.ftn.ncentrala.model.enumeration.BillingType;
import rs.ac.uns.ftn.ncentrala.service.JournalService;

@Service
public class CheckPaymentMethodService implements JavaDelegate{

    @Autowired
    JournalService journalService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        String id = (String)delegateExecution.getVariable("id");

        Journal journal = journalService.findById(Long.parseLong(id));
        if(journal.getBillingType() == BillingType.AUTHORS){
            delegateExecution.setVariable("billingType", "AUTHORS");
        }else{
            delegateExecution.setVariable("billingType", "READERS");
        }
    }
}
