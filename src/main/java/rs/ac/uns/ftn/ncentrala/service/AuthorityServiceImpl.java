package rs.ac.uns.ftn.ncentrala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Authority;
import rs.ac.uns.ftn.ncentrala.repository.jpa.AuthorityRepository;

@Service
public class AuthorityServiceImpl implements AuthorityService {

    @Autowired
    AuthorityRepository authorityRepository;

    @Override
    public Authority save(Authority authority){
        return this.authorityRepository.save(authority);
    }

    @Override
    public Authority findByType(String type){
        return this.authorityRepository.findByType(type);
    }

}
