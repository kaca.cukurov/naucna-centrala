package rs.ac.uns.ftn.ncentrala.service;

import rs.ac.uns.ftn.ncentrala.model.AccountAuthority;

public interface AccountAuthorityService {

    AccountAuthority save(AccountAuthority accountAuthority);
}
