package rs.ac.uns.ftn.ncentrala.service.camunda;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Account;
import rs.ac.uns.ftn.ncentrala.model.AccountAuthority;
import rs.ac.uns.ftn.ncentrala.model.Authority;
import rs.ac.uns.ftn.ncentrala.service.AccountAuthorityService;
import rs.ac.uns.ftn.ncentrala.service.AccountService;
import rs.ac.uns.ftn.ncentrala.service.AuthorityService;

import java.util.ArrayList;
import java.util.List;

@Service
public class RegistrationService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    AccountService accountService;

    @Autowired
    AuthorityService authorityService;

    @Autowired
    AccountAuthorityService accountAuthorityService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        
        User user = identityService.newUser("new");
        Account account = new Account();
        user.setId((String)delegateExecution.getVariable("username"));
        account.setUsername((String)delegateExecution.getVariable("username"));
        user.setPassword((String)delegateExecution.getVariable("password"));
        account.setPassword((String)delegateExecution.getVariable("password"));
        user.setEmail((String)delegateExecution.getVariable("email"));
        account.setEmail((String)delegateExecution.getVariable("email"));
        user.setFirstName((String)delegateExecution.getVariable("firstName"));
        account.setFirstName((String)delegateExecution.getVariable("firstName"));
        user.setLastName((String)delegateExecution.getVariable("lastName"));
        account.setLastName((String)delegateExecution.getVariable("lastName"));
        account.setCity((String)delegateExecution.getVariable("city"));
        account.setCountry((String)delegateExecution.getVariable("country"));
        account.setTitle((String)delegateExecution.getVariable("title"));

        if(this.accountService.findByUsername(account.getUsername()) != null){
            delegateExecution.setVariable("podaciIspravni", false);
            return;
        }

        BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
        account.setPassword(bc.encode(account.getPassword()));

        account = accountService.save(account);
        Authority authority = this.authorityService.findByType("AUTHOR");
        AccountAuthority accountAuthority  = new AccountAuthority();
        accountAuthority.setAccount(account);
        accountAuthority.setAuthority(authority);
        accountAuthority = this.accountAuthorityService.save(accountAuthority);

        delegateExecution.setVariable("podaciIspravni", true);
        identityService.saveUser(user);
        List<AccountAuthority> authorities = new ArrayList<>();
        authorities.add(accountAuthority);
        account.setAccountAuthorities(authorities);
        this.accountService.save(account);
    }
}
