package rs.ac.uns.ftn.ncentrala.service.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Account;
import rs.ac.uns.ftn.ncentrala.model.Coauthor;
import rs.ac.uns.ftn.ncentrala.model.Journal;
import rs.ac.uns.ftn.ncentrala.model.SciencePaper;
import rs.ac.uns.ftn.ncentrala.service.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Service
public class SavePaperAfterReplyService implements JavaDelegate {

    @Autowired
    SciencePaperService sciencePaperService;

    @Autowired
    AccountService accountService;

    @Autowired
    CoauthorService coauthorService;

    @Autowired
    EmailService emailService;

    @Autowired
    JournalService journalService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        LinkedHashMap<String, Object> paperDto = (LinkedHashMap<String, Object>)delegateExecution.getVariable("paperDto");
        SciencePaper sciencePaper = this.sciencePaperService.getByTitle((String)paperDto.get("title"));
        List<Coauthor> coauthors = this.coauthorService.findBySciencePaperId(sciencePaper.getId());
        this.coauthorService.deleteAll(coauthors);
        sciencePaper.setKeyWords((String)paperDto.get("keyWords"));
        sciencePaper.setJournalName((String)paperDto.get("journalName"));
        sciencePaper.setPaperAbstract((String)paperDto.get("paperAbstract"));
        sciencePaper.setScienceField((String)paperDto.get("scienceField"));
        sciencePaper.setFilePath((String)delegateExecution.getVariable("filePath"));
        sciencePaper.setCoauthors(new ArrayList<>());
        sciencePaper = this.sciencePaperService.save(sciencePaper);
        List<LinkedHashMap<String, Object>> coauthorDtos = (List<LinkedHashMap<String, Object>>)paperDto.get("coauthorDtos");
        for(LinkedHashMap<String, Object> mapa: coauthorDtos) {
            Coauthor coauthor = new Coauthor();
            coauthor.setCity((String)mapa.get("city"));
            coauthor.setCountry((String)mapa.get("country"));
            coauthor.setEmail((String)mapa.get("email"));
            coauthor.setFirstName((String)mapa.get("firstName"));
            coauthor.setLastName((String)mapa.get("lastName"));
            coauthor.setMain(false);
            coauthor.setSciencePaper(sciencePaper);
            sciencePaper.getCoauthors().add(coauthor);
        }
        // main author who is logged in
        Account account = this.accountService.findByUsername((String)delegateExecution.getVariable("username"));
        Coauthor coauthor = new Coauthor();
        coauthor.setMain(true);
        coauthor.setCity(account.getCity());
        coauthor.setCountry(account.getCountry());
        coauthor.setEmail(account.getEmail());
        coauthor.setFirstName(account.getFirstName());
        coauthor.setLastName(account.getLastName());
        coauthor.setSciencePaper(sciencePaper);
        sciencePaper.getCoauthors().add(coauthor);
        this.sciencePaperService.save(sciencePaper);

        // send mail to first author
        this.emailService.sendEmailToAuthors(account, sciencePaper);

        // send mail to editor
        String editor = (String)delegateExecution.getVariable("editor");
        Account editorAc = this.accountService.findByUsername(editor);
        this.emailService.toEditorAfterReply(editorAc, sciencePaper);

        delegateExecution.setVariable("author", account.getUsername());
        delegateExecution.setVariable("paperTitle", sciencePaper.getTitle());
    }
}
