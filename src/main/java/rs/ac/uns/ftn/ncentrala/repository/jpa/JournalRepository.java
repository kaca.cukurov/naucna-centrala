package rs.ac.uns.ftn.ncentrala.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ncentrala.model.Journal;

public interface JournalRepository extends JpaRepository<Journal, Long> {

    Journal findByTitle(String title);
}
