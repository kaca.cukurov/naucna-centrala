package rs.ac.uns.ftn.ncentrala.repository.elasticsearch;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.ncentrala.model.SciencePaperES;

@Repository
public interface SciencePaperESRepository extends ElasticsearchRepository<SciencePaperES, String> {

    SciencePaperES findByScienceField(String scienceFields);
}
