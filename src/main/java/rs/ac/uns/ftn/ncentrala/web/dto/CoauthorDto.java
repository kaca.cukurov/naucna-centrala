package rs.ac.uns.ftn.ncentrala.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.uns.ftn.ncentrala.model.Coauthor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CoauthorDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String city;
    private String country;
    private String email;
    private double latitude;
    private double longitude;

    public CoauthorDto(Coauthor coauthor){
        this.firstName = coauthor.getFirstName();
        this.lastName = coauthor.getLastName();
        this.city = coauthor.getCity();
        this.country = coauthor.getCountry();
        this.email = coauthor.getEmail();
    }
}
