package rs.ac.uns.ftn.ncentrala.conf;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
@EnableJpaRepositories(basePackages = {"rs.ac.uns.ftn.ncentrala.repository.jpa"})
@EnableElasticsearchRepositories(basePackages = "rs.ac.uns.ftn.ncentrala.repository.elasticsearch")
public class ElasticSearchConfiguration {

    @Bean
    public Client client(){
        TransportClient client = null;
        try{
            final Settings settings = Settings.builder().put("client.transport.sniff", true)
                    .put("cluster.name", "my-magazine").build();
            client = new PreBuiltTransportClient(settings);
            client.addTransportAddress(new TransportAddress(InetAddress.getByName("127.0.0.1"), 9300));
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
        return client;
    }


}
