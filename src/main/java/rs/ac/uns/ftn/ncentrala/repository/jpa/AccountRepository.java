package rs.ac.uns.ftn.ncentrala.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ncentrala.model.Account;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {

   Account findByUsername(String username);


}
