package rs.ac.uns.ftn.ncentrala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Review;
import rs.ac.uns.ftn.ncentrala.repository.jpa.ReviewRepository;

import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    ReviewRepository reviewRepository;

    @Override
    public Review save(Review review){
        return this.reviewRepository.save(review);
    }

    @Override
    public Review findById(Long id){
        return this.reviewRepository.getOne(id);
    }

    @Override
    public List<Review> findBySciencePaperId(Long id){
        return this.reviewRepository.findBySciencePaperId(id);
    }

    @Override
    public  List<Review> findByReviewerId(Long id){
        return this.reviewRepository.findByReviewerId(id);
    }

    @Override
    public void deleteAll(Iterable<Review> iterable){
        this.reviewRepository.deleteInBatch(iterable);
    }
}
