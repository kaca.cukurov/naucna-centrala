package rs.ac.uns.ftn.ncentrala.web.controller;

import org.camunda.bpm.engine.FormService;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.TaskFormData;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceDto;
import org.camunda.bpm.engine.rest.dto.task.TaskDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import rs.ac.uns.ftn.ncentrala.model.Account;
import rs.ac.uns.ftn.ncentrala.repository.jpa.AccountRepository;
import rs.ac.uns.ftn.ncentrala.security.JWTUtils;
import rs.ac.uns.ftn.ncentrala.service.AccountService;
import rs.ac.uns.ftn.ncentrala.web.dto.FormFieldsDto;
import rs.ac.uns.ftn.ncentrala.web.dto.FormSubmissionDto;
import rs.ac.uns.ftn.ncentrala.web.dto.LoginDTO;
import rs.ac.uns.ftn.ncentrala.web.dto.TokenDTO;

import java.util.List;

@RestController
@RequestMapping(value = "/api/user")
public class UserController {

    @Autowired
    FormService formService;

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    private JWTUtils jwtUtils;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Qualifier("myUserDetailsService")
    @Autowired
    private UserDetailsService userDetailsService;

    private final String path = "http://localhost:8084/rest/";

    @RequestMapping(
            value = "/login",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity login(@RequestBody LoginDTO loginDTO) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    loginDTO.getUsername(), loginDTO.getPassword());
            authenticationManager.authenticate(token);
            Account account = this.accountService.findByUsername(loginDTO.getUsername());

            UserDetails details = userDetailsService.loadUserByUsername(account.getUsername());

            TokenDTO userToken = new TokenDTO(jwtUtils.generateToken(details));

            return new ResponseEntity<>(userToken, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping(path = "/registration", produces = "application/json")
    public @ResponseBody FormFieldsDto startRegistration() {
        // start process
        String startProcessUrl = path + "process-definition/key/"+ "Process_registration" + "/start";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity request = new HttpEntity<>("{}", headers);
        ResponseEntity<ProcessInstanceDto> result = restTemplate.postForEntity(startProcessUrl, request, ProcessInstanceDto.class);
        ProcessInstanceDto pi = result.getBody();

        // get first task
        String taskUrl = path + "task?processInstanceId=" + pi.getId();
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        // get task form
        TaskFormData tfd = formService.getTaskFormData(task.getId());
        List<FormField> properties = tfd.getFormFields();

        return new FormFieldsDto(task.getId(), pi.getId(), properties);
    }

    @PostMapping(path = "/process/{processInstanceId}", produces = "application/json")
    public @ResponseBody ResponseEntity saveData(@RequestBody List<FormSubmissionDto> dto, @PathVariable String processInstanceId) {
        RestTemplate restTemplate = new RestTemplate();

        // get task
        String taskUrl = path + "task?processInstanceId=" + processInstanceId;
        ResponseEntity<TaskDto[]> resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        TaskDto task = resultTask.getBody()[0];

        // submit task form
        String submitUrl = path + "/task/" + task.getId() + "/submit-form";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity request = new HttpEntity<>(JsonFromList(dto), headers);
        restTemplate.postForEntity(submitUrl, request, String.class);

        // get all tasks
        resultTask = restTemplate.getForEntity(taskUrl, TaskDto[].class);
        if(resultTask.getBody().length == 0)
            return new ResponseEntity<>(HttpStatus.OK);
        else{
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    public static String JsonFromList(List<FormSubmissionDto> list){
        String json = "{ \"variables\" : {";
        for(FormSubmissionDto dto : list){
            json += "\"" + dto.getField() + "\": { \"value\":\"" + dto.getValue() + "\"},";
        }
        json = json.substring(0, json.length()-2);
        json += "}}}";
        return json;
    }
}
