package rs.ac.uns.ftn.ncentrala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Account;
import rs.ac.uns.ftn.ncentrala.repository.jpa.AccountRepository;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Account save(Account account){
        return this.accountRepository.save(account);
    }

    @Override
    public Account findByUsername(String username){
        return this.accountRepository.findByUsername(username);
    }

    @Override
    public Account findById(Long id){
        return this.accountRepository.getOne(id);
    }
}
