package rs.ac.uns.ftn.ncentrala.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.uns.ftn.ncentrala.model.Coauthor;
import rs.ac.uns.ftn.ncentrala.model.SciencePaper;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SciencePaperDto {

    private String title;
    private String keyWords;
    private String paperAbstract;

    public SciencePaperDto(SciencePaper sciencePaper){
        this.title = sciencePaper.getTitle();
        this.keyWords = sciencePaper.getKeyWords();
        this.paperAbstract = sciencePaper.getPaperAbstract();
    }
}
