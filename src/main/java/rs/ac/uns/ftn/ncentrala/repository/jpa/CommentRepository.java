package rs.ac.uns.ftn.ncentrala.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ncentrala.model.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    Comment findBySciencePaperId(Long id);
}
