package rs.ac.uns.ftn.ncentrala.service;

import rs.ac.uns.ftn.ncentrala.model.Journal;

import java.util.List;

public interface JournalService {

    Journal save(Journal journal);

    List<Journal> getAll();

    Journal findById(Long id);

    Journal findByTitle(String title);
}
