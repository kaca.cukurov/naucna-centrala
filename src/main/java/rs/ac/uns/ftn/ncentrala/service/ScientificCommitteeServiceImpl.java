package rs.ac.uns.ftn.ncentrala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.ScientificCommittee;
import rs.ac.uns.ftn.ncentrala.repository.jpa.ScientificCommitteeRepository;

@Service
public class ScientificCommitteeServiceImpl implements ScientificCommitteeService {

    @Autowired
    ScientificCommitteeRepository committeeRepository;

    @Override
    public ScientificCommittee save(ScientificCommittee committee){
        return this.committeeRepository.save(committee);
    }
}
