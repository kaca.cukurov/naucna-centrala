package rs.ac.uns.ftn.ncentrala.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.elasticsearch.common.geo.GeoPoint;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class NewReviewerESDto {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private List<String> scienceFieldList;
    private List<String> paperIds;
    private GeoPoint location;
}
