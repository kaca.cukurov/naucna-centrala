package rs.ac.uns.ftn.ncentrala.service;

import rs.ac.uns.ftn.ncentrala.model.Reviewer;

import java.util.List;

public interface ReviewerService {

    Reviewer save(Reviewer reviewer);

    List<Reviewer> getAll();

    Reviewer getOne(String id);
}
