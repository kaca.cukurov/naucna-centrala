package rs.ac.uns.ftn.ncentrala.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.elasticsearch.common.geo.GeoPoint;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.GeoPointField;

import javax.persistence.Id;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = ReviewerES.INDEX_NAME, type = ReviewerES.TYPE_NAME, shards = 1, replicas = 0)
public class ReviewerES {

    static final String INDEX_NAME = "reviewers";
    static final String TYPE_NAME = "reviewers";

    @Id
    @Field(type = FieldType.Text, index = false, store = true)
    private String id;

    @Field(type = FieldType.Text, store = true)
    private String firstName;

    @Field(type = FieldType.Text, store = true)
    private String lastName;

    @Field(type = FieldType.Text, store = true)
    private String email;

    @GeoPointField
    private GeoPoint location;

    @Field(type = FieldType.Text, store = true)
    private Collection<String> scienceFieldList;

    @Field(type = FieldType.Nested, store = true)
    private List<SciencePaperES> papers;


}
