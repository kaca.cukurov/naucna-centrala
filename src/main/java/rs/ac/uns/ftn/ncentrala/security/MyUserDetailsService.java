package rs.ac.uns.ftn.ncentrala.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Account;
import rs.ac.uns.ftn.ncentrala.service.AccountService;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private AccountService accountService;

    @Override
    public UserDetails loadUserByUsername(String username) {
        Account account = null;
        try {
            account = this.accountService.findByUsername(username);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (account == null) {
            throw new UsernameNotFoundException(username);
        }
        return new UserWithRole(account.getId().toString(), account.getUsername(), account.getPassword(), account.getAccountAuthorities().get(0).getAuthority().getType());
    }
}
