package rs.ac.uns.ftn.ncentrala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.Account;
import rs.ac.uns.ftn.ncentrala.model.SciencePaper;


@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private Environment env;

    private static final String URL = "http://localhost:4204/";
    private static final String EMAIL_START = "Dear ";
    private static final String EMAIL_USERNAME = "spring.mail.username";

    @Async
    public void sendEmailToAuthors(Account account, SciencePaper sciencePaper) throws MailException, InterruptedException {

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(account.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + account.getFirstName());
        mail.setText(EMAIL_START + account.getFirstName() + " " + account.getLastName() + ",\n\nYour science paper: "
                + sciencePaper.getTitle().toUpperCase() + " has been submitted and sent to editor in chief of magazine "
                + sciencePaper.getJournalName());
        javaMailSender.send(mail);
    }

    @Async
    public void sendEmailToChiefEditor(Account account, SciencePaper sciencePaper) throws MailException, InterruptedException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(account.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + account.getFirstName());
        mail.setText(EMAIL_START + account.getFirstName() + " " + account.getLastName() + ",\n\nYour have received new science paper: "
                + sciencePaper.getTitle().toUpperCase() + ".");
        javaMailSender.send(mail);
    }

    @Async
    public void declinePaperAuthor(Account account, SciencePaper sciencePaper) throws MailException, InterruptedException {

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(account.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + account.getFirstName());
        mail.setText(EMAIL_START + account.getFirstName() + " " + account.getLastName() + ",\n\nYour science paper: "
                + sciencePaper.getTitle().toUpperCase() + " has been declined from journal:  "
                + sciencePaper.getJournalName());
        javaMailSender.send(mail);
    }

    @Async
    public void declinePaperATime(Account account, SciencePaper sciencePaper) throws MailException, InterruptedException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(account.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + account.getFirstName());
        mail.setText(EMAIL_START + account.getFirstName() + " " + account.getLastName() + ",\n\nYour science paper: "
                + sciencePaper.getTitle().toUpperCase() + " has been declined, because you haven't uploaded new version " +
                "within the set time limit, from journal:  "
                + sciencePaper.getJournalName());
        javaMailSender.send(mail);
    }

    @Async
    public void changeFormat(Account account, SciencePaper sciencePaper, String comment) throws MailException, InterruptedException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(account.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + account.getFirstName());
        mail.setText(EMAIL_START + account.getFirstName() + " " + account.getLastName() + ",\n\nYour science paper: "
                + sciencePaper.getTitle().toUpperCase() + " has been read by editor in chief, and format " +
                "changes were requested. This is his/hers comment: \n"  + comment);
        javaMailSender.send(mail);
    }

    @Async
    public void editorToChooseReviewers(Account account, SciencePaper sciencePaper) throws MailException, InterruptedException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(account.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + account.getFirstName());
        mail.setText(EMAIL_START + account.getFirstName() + " " + account.getLastName() + ",\n\nYou have been selected " +
                "to choose reviewers for science paper: " + sciencePaper.getTitle().toUpperCase());
        javaMailSender.send(mail);
    }

    @Async
    public void notifyReviewers(Account account, SciencePaper sciencePaper) throws MailException, InterruptedException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(account.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + account.getFirstName());
        mail.setText(EMAIL_START + account.getFirstName() + " " + account.getLastName() + ",\n\nYou have been selected " +
                "to review science paper: " + sciencePaper.getTitle().toUpperCase());
        javaMailSender.send(mail);
    }

    @Async
    public void notifyEditorReviewrsAreDone(Account account, SciencePaper sciencePaper) throws MailException, InterruptedException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(account.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + account.getFirstName());
        mail.setText(EMAIL_START + account.getFirstName() + " " + account.getLastName() + ",\n\nAll reviewers are done " +
                "with reviewing science paper: " + sciencePaper.getTitle().toUpperCase());
        javaMailSender.send(mail);
    }

    @Async
    public void acceptPaper(Account account, SciencePaper sciencePaper) throws MailException, InterruptedException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(account.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + account.getFirstName());
        mail.setText(EMAIL_START + account.getFirstName() + " " + account.getLastName() + ",\n\nYour science paper " +
                sciencePaper.getTitle().toUpperCase() + " has been accepted!");
        javaMailSender.send(mail);
    }

    @Async
    public void minorChagnes(Account account, SciencePaper sciencePaper) throws MailException, InterruptedException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(account.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + account.getFirstName());
        mail.setText(EMAIL_START + account.getFirstName() + " " + account.getLastName() + ",\n\nChanges were " +
                "requested for your science paper: " + sciencePaper.getTitle().toUpperCase());
        javaMailSender.send(mail);
    }

    @Async
    public void toEditorAfterReply(Account account, SciencePaper sciencePaper) throws MailException, InterruptedException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(account.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + account.getFirstName());
        mail.setText(EMAIL_START + account.getFirstName() + " " + account.getLastName() + ",\n\nScience paper: " +
                sciencePaper.getTitle().toUpperCase() + " has been resubmitted by author.");
        javaMailSender.send(mail);
    }

    @Async
    public void reviewersFailed(Account account, SciencePaper sciencePaper, String names) throws MailException, InterruptedException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(account.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + account.getFirstName());
        mail.setText(EMAIL_START + account.getFirstName() + " " + account.getLastName() + ",\n\nThese reviewers: "
                + names + "failed to review for science paper: " + sciencePaper.getTitle().toUpperCase());
        javaMailSender.send(mail);
    }

}
