package rs.ac.uns.ftn.ncentrala;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import rs.ac.uns.ftn.ncentrala.service.InitDBService;

@SpringBootApplication
@EnableProcessApplication
public class NcentralaApplication {

	public static void main(String[] args){
		ConfigurableApplicationContext context = SpringApplication.run(NcentralaApplication.class, args);
		//context.getBean(InitDBService.class).init();
		//context.getBean(InitDBService.class).initES();
	}
}

