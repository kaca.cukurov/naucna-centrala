package rs.ac.uns.ftn.ncentrala.service.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.ncentrala.model.*;
import rs.ac.uns.ftn.ncentrala.service.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class AcceptFinalPaper implements JavaDelegate{
    @Autowired
    EmailService emailService;

    @Autowired
    SciencePaperService sciencePaperService;

    @Autowired
    AccountService accountService;

    @Autowired
    ReviewerESService reviewerESService;

    @Autowired
    SciencePaperESService sciencePaperESService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        Account account = this.accountService.findByUsername((String)delegateExecution.getVariable("author"));
        SciencePaper paper = this.sciencePaperService.getByTitle((String)delegateExecution.getVariable("paperTitle"));
        this.emailService.acceptPaper(account, paper);


        SciencePaperES sciencePaperES = new SciencePaperES();
        sciencePaperES.setId(paper.getId().toString());
        sciencePaperES.setText("");
        List<Coauthor> coauthors = paper.getCoauthors();
        for(Coauthor coauthor:coauthors)
            coauthor.setSciencePaper(null);
        sciencePaperES.setCoauthors(coauthors);
        sciencePaperES.setTitle(paper.getTitle());
        sciencePaperES.setFilePath(paper.getFilePath());
        sciencePaperES.setScienceField(paper.getScienceField());
        sciencePaperES.setKeyWords(paper.getKeyWords());
        sciencePaperES.setPaperAbstract(paper.getPaperAbstract());
        sciencePaperES.setJournalName(paper.getJournalName());
        Random rand = new Random();
        int i = rand.nextInt(10000) + 1;
        int j = rand.nextInt(10000) + 1;
        sciencePaperES.setDoi("10." + i + "/" + j);

        sciencePaperES = this.sciencePaperESService.save(sciencePaperES);

        String all = (String)delegateExecution.getVariable("all");
        List<String> reviewers = new ArrayList<>();
        String[] usernames = all.split(",");
        for(String user: usernames){
            Account account1 = this.accountService.findByUsername(user);
            ReviewerES reviewerES1 = this.reviewerESService.findOne(account1.getId().toString());
            if(reviewerES1.getPapers() == null)
                reviewerES1.setPapers(new ArrayList<>());
            reviewerES1.getPapers().add(sciencePaperES);
            this.reviewerESService.save(reviewerES1);
        }

    }

}
