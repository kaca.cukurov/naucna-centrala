import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from "../shared/shared.module";
import {ToasterModule} from "angular5-toaster/dist";
import {SellingRouterModule} from "./selling-router.module";
import { SellPapersComponent } from './sell-papers/sell-papers.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ToasterModule,
    SellingRouterModule
  ],
  declarations: [SellPapersComponent]
})
export class SellingModule { }
