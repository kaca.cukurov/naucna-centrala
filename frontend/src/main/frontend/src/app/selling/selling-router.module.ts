import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {SellPapersComponent} from "./sell-papers/sell-papers.component";


const routes: Routes = [
  { path: '', component: SellPapersComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SellingRouterModule { }
