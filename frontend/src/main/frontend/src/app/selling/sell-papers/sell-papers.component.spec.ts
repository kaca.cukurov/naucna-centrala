import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellPapersComponent } from './sell-papers.component';

describe('SellPapersComponent', () => {
  let component: SellPapersComponent;
  let fixture: ComponentFixture<SellPapersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellPapersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellPapersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
