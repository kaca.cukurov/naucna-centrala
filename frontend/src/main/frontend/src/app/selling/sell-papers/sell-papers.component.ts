import { Component, OnInit } from '@angular/core';
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {Router} from "@angular/router";
import {sepPaper} from "../../shared/models/sepPaper";
import {magazineEdition} from "../../shared/models/magazineEditon";
import {SepService} from "../../core/services/sep.service";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {AppError} from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";

@Component({
  selector: 'app-sell-papers',
  templateUrl: './sell-papers.component.html',
  styleUrls: ['./sell-papers.component.css']
})
export class SellPapersComponent implements OnInit {

  toasterConfig : ToasterConfig;
  allPapers: Array<sepPaper>;
  allEditions: Array<magazineEdition>;
  url: string;
  customerId: string;
  urls: string;

  constructor(private router:Router,  private toasterService: ToasterService, private sepService: SepService) {
    this.toasterConfig = new ToasterConfig({timeout: 4000});
  }

  ngOnInit() {
    this.allPapers = [];
    this.allEditions = [];
    this.loadData();
    this.loadUrl();
    this.loadCustomerId();
  }

  loadCustomerId(){
    this.sepService.getCustomerId().subscribe(data => {
      this.customerId = data.url;
    }, (error: AppError) => {
      this.handleError(error);
    });
  }

  loadUrl(){
    this.sepService.getFrontUrl().subscribe(data => {
      this.url = data.url;
      this.urls = '&successUrl=' + this.url + '\/selling' + '&cancelUrl=' + this.url + '\/++selling';
    }, (error: AppError) => {
      this.handleError(error);
    });
  }

  createPlan(){
    let parameters = '?customerId=' + this.customerId + this.urls;
    this.sepService.getUrl().subscribe(data => {
      window.location.href = '//' + data.url + 'create_plan' + parameters;
    }, (error: AppError) => {
        this.handleError(error);
    });
  }

  buyPaper(paper){
    let parameters = '?customerId=' + this.customerId + '&price=' + paper.price + '&currency=' + paper.currency + this.urls;
    this.sepService.getUrl().subscribe(data => {
      window.location.href = '//' + data.url + 'home' + parameters;
    }, (error: AppError) => {
      this.handleError(error);
    });

  }

  buyEdition(edition){
    let parameters = '?customerId=' + this.customerId + '&price=' + edition.price + '&currency=' + edition.currency + this.urls;
    this.sepService.getUrl().subscribe(data => {
      window.location.href = '//' + data.url + 'home' + parameters;
    }, (error: AppError) => {
      this.handleError(error);
    });
  }

  subscribeCall(){
    let parameters = '?customerId=' + this.customerId + '&returnUrl=' + this.url + '\/selling';
    this.sepService.getUrl().subscribe(data => {
      window.location.href = '//' + data.url + 'subscribe' + parameters;
    }, (error: AppError) => {
      this.handleError(error);
    });
  }

  loadData(){
    let paper1 = new sepPaper();
    paper1.name = "Science paper about plants";
    paper1.price = 5;
    paper1.currency = "USD";
    this.allPapers.push(paper1);

    let paper2 = new sepPaper();
    paper2.name = "Science paper about animals";
    paper2.price = 10;
    paper2.currency = "USD";
    this.allPapers.push(paper2);

    let paper3 = new sepPaper();
    paper3.name = "Science paper about architecture";
    paper3.price = 15;
    paper3.currency = "USD";
    this.allPapers.push(paper3);

    let paper4 = new sepPaper();
    paper4.name = "Science paper about technology";
    paper4.price = 20;
    paper4.currency = "USD";
    this.allPapers.push(paper4);

    let paper5 = new sepPaper();
    paper5.name = "Science paper about medicine";
    paper5.price = 25;
    paper5.currency = "USD";
    this.allPapers.push(paper5);

    let paper6 = new sepPaper();
    paper6.name = "Science paper about nuclear power";
    paper6.price = 30;
    paper6.currency = "USD";
    this.allPapers.push(paper6);

    let edition1 = new magazineEdition();
    edition1.name = "Edition about plants, animals and architecture";
    edition1.papers = [paper1, paper2, paper3];
    edition1.price = 65;
    edition1.currency = "USD";
    this.allEditions.push(edition1);

    let edition2 = new magazineEdition();
    edition2.name = "Edition about technology, medicine and nuclear power";
    edition2.papers = [paper4, paper5, paper6];
    edition2.price = 80;
    edition2.currency = "USD";
    this.allEditions.push(edition2);
  }

  handleError(error: AppError) {
    if (error instanceof NotFoundError)
      this.toasterService.pop('error', 'Error', 'Not found!');
    else if (error instanceof ForbiddenError)
      this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
    else if (error instanceof BadRequestError)
      this.toasterService.pop('error', 'Error', 'Bad request!');
    else {
      this.toasterService.pop('error', 'Error', 'Error, look at console!');
      throw error;
    }
  }

}
