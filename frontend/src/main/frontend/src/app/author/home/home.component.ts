import { Component, OnInit } from '@angular/core';
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {EditorService} from "../../core/services/editor.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PaperService} from "../../core/services/paper.service";
import {taskDto} from "../../shared/models/taskDto";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {AppError} from "../../shared/errors/app-error";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  taskList: Array<taskDto> = [];
  toasterConfig : ToasterConfig;

  constructor(private router:Router, private paperService: PaperService, private toasterService: ToasterService,
              private route: ActivatedRoute, private editorService: EditorService) {
    this.toasterConfig = new ToasterConfig({timeout: 4000});
  }

  ngOnInit() {
    this.getTasks();
  }


  getTasks(){
    this.editorService.getTasks().subscribe(data =>{
      this.taskList = data;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'You can not upload that file');
        throw error;
      }
    });
  }

  nextStep(task: taskDto){
    console.log(task.name);
    if(task.name == "Ispravka formata rada"){
      this.router.navigate(['papers/reupload', {id: task.id, name: task.name, title: task.paperTitle, processId: task.processId}])
    }else if(task.name == "Ispravka rada"){
      this.router.navigate(['author/reply', {id: task.id, name: task.name, title: task.paperTitle, processId: task.processId}])
    }
  }

}
