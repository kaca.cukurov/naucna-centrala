import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToasterModule} from "angular5-toaster/dist";
import {SharedModule} from "../shared/shared.module";
import {HomeComponent} from "./home/home.component";
import {AuthorRouterModule} from "./author-router.module";
import { ReplyCommentComponent } from './reply-comment/reply-comment.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AuthorRouterModule,
    ToasterModule
  ],
  declarations: [
    HomeComponent,
    ReplyCommentComponent
  ]
})
export class AuthorModule { }
