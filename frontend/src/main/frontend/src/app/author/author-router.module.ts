import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from './home/home.component';
import {ReplyCommentComponent} from "./reply-comment/reply-comment.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'reply', component: ReplyCommentComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AuthorRouterModule { }
