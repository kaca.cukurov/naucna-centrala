import { Component, OnInit } from '@angular/core';
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {UserService} from "../core/services/user.service";
import {Router} from "@angular/router";
import {formFieldsDto} from "../shared/models/FormFieldsDto";
import {AppError} from "../shared/errors/app-error";
import {NotFoundError} from "../shared/errors/not-found-error";
import {ForbiddenError} from "../shared/errors/forbidden-error";
import {BadRequestError} from "../shared/errors/bad-request-error";
import {formSubmissionDto} from "../shared/models/formSubmissionDto";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  formFieldsDto: formFieldsDto;
  submissionForm: Array<formSubmissionDto>;

  toasterConfig : ToasterConfig;

  constructor(private router:Router, private userService: UserService, private toasterService: ToasterService) {
    this.toasterConfig = new ToasterConfig({timeout: 4000});
  }

  ngOnInit() {
    this.startProcess();
  }

  register(){
    console.log(this.submissionForm);
    this.userService.register(this.formFieldsDto.processInstanceId, this.submissionForm).toPromise().then(() => {
      this.router.navigate(['']);
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Username exist', 'Change username!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  startProcess(){
    this.userService.startRegistrationProcess().subscribe(data => {
      console.log("Started new process: " + data.processInstanceId);
      this.formFieldsDto = data;
      this.submissionForm = [];
      for(let form of this.formFieldsDto.formFields){
        let submission = new formSubmissionDto();
        submission.field = form.id;
        submission.value = "";
        this.submissionForm.push(submission);
      }
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

}
