import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {JwtService} from "../services/jwt.service";

@Injectable()
export class AnonymousGuard implements CanActivate {
  constructor(private router: Router, private jwtService: JwtService){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot){

    return !(this.jwtService.tokenExist() && !this.jwtService.isTokenExpired());
  }
}
