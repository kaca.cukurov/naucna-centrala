import { TestBed, async, inject } from '@angular/core/testing';

import { ChiefGuard } from './chief.guard';

describe('ChiefGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChiefGuard]
    });
  });

  it('should ...', inject([ChiefGuard], (guard: ChiefGuard) => {
    expect(guard).toBeTruthy();
  }));
});
