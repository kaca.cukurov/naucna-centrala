import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AppError} from "../../shared/errors/app-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ConflictError} from "../../shared/errors/conflict-error";
import {Observable} from "rxjs/Observable";
import {taskDto} from "../../shared/models/taskDto";

@Injectable()
export class EditorService {

  private readonly urlBase = '/api/editor';

  constructor(private http: HttpClient) { }

  getTasks(): Observable<Array<taskDto>>{
    return this.http.get(`${this.urlBase}/tasks`).catch(this.handleErrors);
  }


  protected handleErrors(response: Response) {
    if(response.status === 400)
      return Observable.throw(new BadRequestError());
    else if(response.status === 403)
      return Observable.throw(new ForbiddenError());
    else if(response.status === 404)
      return Observable.throw(new NotFoundError());
    else if(response.status === 409)
      return Observable.throw(new ConflictError());
    return Observable.throw(new AppError(response));
  }

}
