import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ConflictError} from "../../shared/errors/conflict-error";
import {AppError} from "../../shared/errors/app-error";
import {pathDTO} from "../../shared/models/pathDTO";

@Injectable()
export class SepService {


  private readonly urlBase = '/api/sep';

  constructor(private http: HttpClient) { }

  getUrl():Observable<pathDTO> {
    return this.http.get<pathDTO>(`${this.urlBase}/getUrl`).catch(this.handleErrors);
  }

  getFrontUrl(): Observable<pathDTO>{
    return this.http.get<pathDTO>(`${this.urlBase}/getFrontUrl`).catch(this.handleErrors);
  }

  getCustomerId(): Observable<pathDTO>{
    return this.http.get<pathDTO>(`${this.urlBase}/getCustomerId`).catch(this.handleErrors);
  }

  protected handleErrors(response: Response) {
    if(response.status === 400)
      return Observable.throw(new BadRequestError());
    else if(response.status === 403)
      return Observable.throw(new ForbiddenError());
    else if(response.status === 404)
      return Observable.throw(new NotFoundError());
    else if(response.status === 409)
      return Observable.throw(new ConflictError());
    return Observable.throw(new AppError(response));
  }

}
