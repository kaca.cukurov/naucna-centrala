import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {Observable} from "rxjs/Observable";
import {ConflictError} from "../../shared/errors/conflict-error";
import {journalDto} from "../../shared/models/journalDto";
import {AppError} from "../../shared/errors/app-error";

@Injectable()
export class JournalService {


  private readonly urlBase = '/api/journal';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Array<journalDto>>{
    return this.http.get<Array<journalDto>>(`${this.urlBase}/all`).catch(this.handleErrors);
  }

  protected handleErrors(response: Response) {
    if(response.status === 400)
      return Observable.throw(new BadRequestError());
    else if(response.status === 403)
      return Observable.throw(new ForbiddenError());
    else if(response.status === 404)
      return Observable.throw(new NotFoundError());
    else if(response.status === 409)
      return Observable.throw(new ConflictError());
    return Observable.throw(new AppError(response));
  }

}
