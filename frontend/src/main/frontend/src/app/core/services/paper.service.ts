import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ConflictError} from "../../shared/errors/conflict-error";
import {queryDTO} from "../../shared/models/queryDTO";
import {AppError} from "../../shared/errors/app-error";
import {searchSciencePaperESDTO} from "../../shared/models/searchSciencePaperESDTO";
import {reviewerPaperDto} from "../../shared/models/reviewerPaperDto";
import {formFieldsDto} from "../../shared/models/FormFieldsDto";
import {formSubmissionDto} from "../../shared/models/formSubmissionDto";
import {sciencePaperDto} from "../../shared/models/sciencePaperDto";
import {commentDto} from "../../shared/models/commentDto";
import {reviewCommentsDto} from "../../shared/models/reviewCommentsDto";
import {authorsDto} from "../../shared/models/authorsDto";
import {coauthorDTO} from "../../shared/models/coauthorDTO";

@Injectable()
export class PaperService {

    private readonly urlBase = '/api/papers';

    constructor(private http: HttpClient) { }

  startPublishingProcess(): Observable<formFieldsDto>{
    return this.http.get<formFieldsDto>(`${this.urlBase}/startPublishingProcess`).catch(this.handleErrors);
  }

  chooseJournal(id: string, form: Array<formSubmissionDto>):Observable<formFieldsDto>{
    return this.http.post<formFieldsDto>(`${this.urlBase}/process/checkPayment/${id}`, form).catch(this.handleErrors);
  }

  searchPapers(queryDTO: queryDTO): Observable<Array<searchSciencePaperESDTO>> {
    return this.http.post<queryDTO>(`${this.urlBase}/search`, queryDTO).catch(this.handleErrors);
  }

  uploadPaper(formData, id:string): Observable<any>{
    return this.http.post<queryDTO>(`${this.urlBase}/upload/${id}`, formData).catch(this.handleErrors);
  }

  finalDecision(body, id:string): Observable<any>{
    return this.http.post<queryDTO>(`${this.urlBase}/finalDecision/${id}`, body).catch(this.handleErrors);
  }

  replyUploadPaper(formData, id:string): Observable<any>{
    return this.http.post<queryDTO>(`${this.urlBase}/uploadReply/${id}`, formData).catch(this.handleErrors);
  }

  getAll(): Observable<Array<reviewerPaperDto>>{
    return this.http.get<Array<reviewerPaperDto>>(`${this.urlBase}/all`).catch(this.handleErrors);
  }

  getComment(id: string): Observable<authorsDto>{
    return this.http.get<authorsDto>(`${this.urlBase}/getCommentAndAuthors/${id}`).catch(this.handleErrors);
  }

  getCoauthors(id: string): Observable<Array<coauthorDTO>>{
    return this.http.get<Array<coauthorDTO>>(`${this.urlBase}/coauthors/${id}`).catch(this.handleErrors);
  }

  getByTitle(title: string): Observable<sciencePaperDto>{
    return this.http.get(`${this.urlBase}/byTitle/${title}`).catch(this.handleErrors);
  }

  getCommentByTitle(title: string): Observable<commentDto>{
    return this.http.get(`${this.urlBase}/commentByTitle/${title}`).catch(this.handleErrors);
  }

  getReviewCommentByTitle(title: string): Observable<reviewCommentsDto>{
    return this.http.get(`${this.urlBase}/reviewCommentsByTitle/${title}`).catch(this.handleErrors);
  }

  declinePaper(id: string){
    return this.http.post(`${this.urlBase}/process/decline`, id).catch(this.handleErrors);
  }

  acceptPaper(id: string): Observable<string>{
    return this.http.post<string>(`${this.urlBase}/process/accept`, id).catch(this.handleErrors);
  }

  changeFormat(id: string, body){
    return this.http.post(`${this.urlBase}/process/changeFormat/${id}`, body).catch(this.handleErrors);
  }

  acceptFormat(id: string){
    return this.http.post(`${this.urlBase}/process/acceptFormat`, id).catch(this.handleErrors);
  }

  downloadPdf(title: string): Observable<Blob> {
    return this.http.get(`${this.urlBase}/pdf/${title}`, {
      headers: new HttpHeaders({
        'Accept': '*/*, application/pdf, application/json'
      }),
      responseType: 'blob'
    });
  }

  protected handleErrors(response: Response) {
    if(response.status === 400)
      return Observable.throw(new BadRequestError());
    else if(response.status === 403)
      return Observable.throw(new ForbiddenError());
    else if(response.status === 404)
      return Observable.throw(new NotFoundError());
    else if(response.status === 409)
      return Observable.throw(new ConflictError());
    return Observable.throw(new AppError(response));
  }
}
