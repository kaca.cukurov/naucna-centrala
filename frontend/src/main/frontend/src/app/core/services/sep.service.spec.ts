import { TestBed, inject } from '@angular/core/testing';

import { SepService } from './sep.service';

describe('SepService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SepService]
    });
  });

  it('should be created', inject([SepService], (service: SepService) => {
    expect(service).toBeTruthy();
  }));
});
