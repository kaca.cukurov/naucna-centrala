import { Injectable } from '@angular/core';
import {AppError} from "../../shared/errors/app-error";
import {Observable} from "rxjs/Observable";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ConflictError} from "../../shared/errors/conflict-error";
import {HttpClient} from "@angular/common/http";
import {searchReviewerESDTO} from "../../shared/models/searchReviewerESDTO";
import {reviewerDto} from "../../shared/models/reviewerDto";
import {reviewDto} from "../../shared/models/reviewDto";
import {recomDto} from "../../shared/models/recomDto";

@Injectable()
export class ReviewerService {

  private readonly urlBase = '/api/reviewers';

  getAll(title: string, id:string): Observable<Array<reviewerDto>>{
    let body = new FormData();
    body.append("title", title);
    return this.http.post<Array<reviewerDto>>(`${this.urlBase}/all/${id}`, body).catch(this.handleErrors);
  }

  moreLikeThisSearch(title: string): Observable<Array<searchReviewerESDTO>> {
    let body = new FormData();
    body.append("title", title);
    return this.http.post<string>(`${this.urlBase}/search`, body).catch(this.handleErrors);
  }

  geoSearch(title: string): Observable<Array<searchReviewerESDTO>> {
    let body = new FormData();
    body.append("title", title);
    return this.http.post<string>(`${this.urlBase}/geo`, body).catch(this.handleErrors);
  }

  fieldsSearch(title: string): Observable<Array<searchReviewerESDTO>> {
    let body = new FormData();
    body.append("title", title);
    return this.http.post<string>(`${this.urlBase}/field`, body).catch(this.handleErrors);
  }

  setChiefReviewer(id: string){
    return this.http.post(`${this.urlBase}/process/chiefEditor`, id).catch(this.handleErrors);
  }

  setReviewersToReview(id: string, body){
    return this.http.post(`${this.urlBase}/process/setReviewers/${id}`, body).catch(this.handleErrors);
  }

  saveReview(id: string, review: reviewDto){
    return this.http.post(`${this.urlBase}/process/saveReview/${id}`, review).catch(this.handleErrors);
  }

  getRecoms(title: string): Observable<Array<recomDto>>{
    return this.http.get<Array<recomDto>>(`${this.urlBase}/recoms/${title}`).catch(this.handleErrors);
  }

  decide(id: string, decision){
    return this.http.post(`${this.urlBase}/process/decide/${id}`, decision).catch(this.handleErrors);
  }

  constructor(private http: HttpClient) { }

  protected handleErrors(response: Response) {
    if(response.status === 400)
      return Observable.throw(new BadRequestError());
    else if(response.status === 403)
      return Observable.throw(new ForbiddenError());
    else if(response.status === 404)
      return Observable.throw(new NotFoundError());
    else if(response.status === 409)
      return Observable.throw(new ConflictError());
    return Observable.throw(new AppError(response));
  }
}
