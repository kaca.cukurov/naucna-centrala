import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ConflictError} from "../../shared/errors/conflict-error";
import {AppError} from "../../shared/errors/app-error";
import {formFieldsDto} from "../../shared/models/FormFieldsDto";
import {formSubmissionDto} from "../../shared/models/formSubmissionDto";

@Injectable()
export class UserService {
  private readonly urlBase = '/api/user';

  constructor(private http: HttpClient) { }

  startRegistrationProcess(): Observable<formFieldsDto>{
    return this.http.get<formFieldsDto>(`${this.urlBase}/registration`).catch(this.handleErrors);
  }

  register(id: string, form: Array<formSubmissionDto>){
   return this.http.post(`${this.urlBase}/process/${id}`, form).catch(this.handleErrors);
  }

  protected handleErrors(response: Response) {
    if(response.status === 400)
      return Observable.throw(new BadRequestError());
    else if(response.status === 403)
      return Observable.throw(new ForbiddenError());
    else if(response.status === 404)
      return Observable.throw(new NotFoundError());
    else if(response.status === 409)
      return Observable.throw(new ConflictError());
    return Observable.throw(new AppError(response));
  }
}
