import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home/home.component';
import {ToasterModule} from "angular5-toaster/dist";
import {SharedModule} from "../shared/shared.module";
import {ChiefRouterModule} from "./chief-router.module";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ChiefRouterModule,
    ToasterModule
  ],
  declarations: [
    HomeComponent
  ]
})
export class ChiefModule { }
