import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {AnonymousGuard} from "./core/guards/anonymous.guard";
import {RegistrationComponent} from "./registration/registration.component";
import {ChiefGuard} from "./core/guards/chief.guard";
import {EditorGuard} from "./core/guards/editor.guard";

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, canActivate: [AnonymousGuard]},
  { path: 'registration', component: RegistrationComponent, canActivate: [AnonymousGuard]},
  { path: 'papers', loadChildren: 'app/papers/papers.module#PapersModule'},
  { path: 'chief', loadChildren: 'app/chief/chief.module#ChiefModule', canActivate: [ChiefGuard]},
  { path: 'editor', loadChildren: 'app/editor/editor.module#EditorModule'},
  { path: 'author', loadChildren: 'app/author/author.module#AuthorModule'},
  { path: 'reviewers', loadChildren: 'app/reviewers/reviewers.module#ReviewersModule'},
  { path: 'selling', loadChildren: 'app/selling/selling.module#SellingModule', canActivate: [AnonymousGuard]},
  { path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: false})
  ],
  exports: [RouterModule]
})
export class AppRouterModule { }
