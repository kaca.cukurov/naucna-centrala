import {newSciencePaperDto} from "./newSciencePaperDto";

export class reviewCommentsDto{
  reviews: Array<string>;
  sciencePaperDto: newSciencePaperDto;
}
