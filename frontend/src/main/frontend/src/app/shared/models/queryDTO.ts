import {simpleQueryDTO} from "./simpleQueryDTO";

export class queryDTO{
  values: Array<simpleQueryDTO> = [];
  operations: Array<string> = [];
  phrase: boolean;
}
