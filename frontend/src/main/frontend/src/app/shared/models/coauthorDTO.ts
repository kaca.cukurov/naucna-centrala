export class coauthorDTO{
  id: number;
  firstName: string;
  lastName: string;
  city: string;
  country: string;
  email: string;
}
