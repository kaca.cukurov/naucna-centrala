export class sciencePaperDto{
  title: string;
  keyWords: string;
  paperAbstract: string;
}
