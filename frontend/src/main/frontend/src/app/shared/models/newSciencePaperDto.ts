import {coauthorDTO} from "./coauthorDTO";

export class newSciencePaperDto{
  title: string;
  coauthorDtos: Array<coauthorDTO>;
  keyWords: string;
  paperAbstract: string;
  scienceField: string;
  journalName: string;
}
