import {formField} from "./formField";

export class formFieldsDto{
  taskId: string;
  processInstanceId: string;
  formFields: Array<formField>;
}
