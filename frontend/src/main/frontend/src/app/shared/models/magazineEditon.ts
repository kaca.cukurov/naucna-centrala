import {sepPaper} from "./sepPaper";

export class magazineEdition{
  name: string;
  papers: Array<sepPaper>;
  price: number;
  currency: string;
}
