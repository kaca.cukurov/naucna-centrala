export class reviewerDto{
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  scienceFieldList: Array<string>;
}
