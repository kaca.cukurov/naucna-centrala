import {coauthorDTO} from "./coauthorDTO";

export class authorsDto{
  comment: string;
  authors: Array<coauthorDTO>;
}
