export class reviewDto{
  commentAuthor: string;
  commentEditor: string;
  recommendation: string;
  paperTitle: string;
}
