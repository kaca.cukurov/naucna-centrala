export class searchReviewerESDTO{
  firstName: string;
  lastName: string;
  email: string;
  scienceFieldList: Array<string>;
  username: string;
}
