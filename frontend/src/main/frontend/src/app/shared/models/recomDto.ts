export class recomDto{
  paperTitle: string;
  commentEditor: string;
  recommendation: string;
  reviewer: string;
}
