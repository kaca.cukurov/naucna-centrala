export class divPapers{
  userFields : Array<string> = ["Title", "Journal name", "Authors", "Key words", "Paper abstract", "Science field", "Text"];
  operations: Array<string> = ["AND", "OR"];
  firstField: string  = "Title";
  firstOperation: string  = "AND";
  input: string = "";
}
