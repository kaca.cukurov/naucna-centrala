export class divAuthor{
  firstName: string;
  lastName: string;
  city: string;
  country: string;
  email: string;
}
