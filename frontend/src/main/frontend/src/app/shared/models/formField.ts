export class formField{
  businessKey: boolean;
  defaultValue: object;
  id: string;
  label: string;
  properties: object;
  type: object;
  typeName: string;
  validationConstraints: Array<object>;
  value: object;
}
