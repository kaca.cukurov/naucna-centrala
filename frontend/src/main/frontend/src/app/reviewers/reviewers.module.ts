import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from "../shared/shared.module";
import {ToasterModule} from "angular5-toaster/dist";
import {ReviewersRouterModule} from "./reviewers-router.module";
import {SearchReviewersComponent} from './search-reviewers/search-reviewers.component';
import { HomeComponent } from './home/home.component';
import { ReviewComponent } from './review/review.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ToasterModule,
    ReviewersRouterModule
  ],
  declarations: [SearchReviewersComponent, HomeComponent, ReviewComponent]
})
export class ReviewersModule { }
