import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {SearchReviewersComponent} from "./search-reviewers/search-reviewers.component";
import {HomeComponent} from "./home/home.component";
import {ReviewComponent} from "./review/review.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'search', component: SearchReviewersComponent},
  { path: 'review', component: ReviewComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ReviewersRouterModule { }
