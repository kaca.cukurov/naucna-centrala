import {Component, OnInit} from '@angular/core';
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {ActivatedRoute, Router} from "@angular/router";
import {ReviewerService} from "../../core/services/reviewer.service";
import {PaperService} from "../../core/services/paper.service";
import {AppError} from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {taskDto} from "../../shared/models/taskDto";
import {saveAs as importedSaveAs} from 'file-saver';
import {sciencePaperDto} from "../../shared/models/sciencePaperDto";
import {reviewDto} from "../../shared/models/reviewDto";

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  task: taskDto = new taskDto();
  paper: sciencePaperDto;
  review : reviewDto;
  authorComment: string;
  editorComment: string;
  allRecom: Array<string> = [];
  recom: string;

  toasterConfig : ToasterConfig;

  constructor(private router:Router, private reviewerService: ReviewerService, private paperService: PaperService,
              private toasterService: ToasterService, private route: ActivatedRoute) {
    this.task.id = this.route.snapshot.paramMap.get('id');
    this.task.name = this.route.snapshot.paramMap.get('name');
    this.task.paperTitle = this.route.snapshot.paramMap.get('title');
    this.task.processId = this.route.snapshot.paramMap.get('processId');
    this.allRecom = ['Accept', 'Accept with minor corrections', 'Conditionally accepted with more corrections', 'Reject'];
    this.recom = this.allRecom[0];
    this.review = new reviewDto();
  }

  ngOnInit() {
    this.getPaper();
  }

  getPaper(){
    this.paperService.getByTitle(this.task.paperTitle).subscribe(data => {
      this.paper = data;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Username exist', 'Change username!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  download(){
    this.paperService.downloadPdf(this.task.paperTitle).subscribe(
      blob => {
        importedSaveAs(blob, this.task.paperTitle + '.pdf');
      }, err => { });
  }

  send(){
    this.review.commentAuthor = this.authorComment;
    this.review.commentEditor = this.editorComment;
    this.review.recommendation = this.recom;
    this.review.paperTitle = this.task.paperTitle;

    console.log(this.review);

    this.reviewerService.saveReview(this.task.processId, this.review).subscribe(data => {
      this.toasterService.pop('success', 'Success', 'Review saved!');
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Username exist', 'Change username!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

}
