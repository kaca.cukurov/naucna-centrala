import {Component, OnInit} from '@angular/core';
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {searchReviewerESDTO} from "../../shared/models/searchReviewerESDTO";
import {ActivatedRoute, Router} from "@angular/router";
import {ReviewerService} from "../../core/services/reviewer.service";
import {PaperService} from "../../core/services/paper.service";
import {AppError} from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {taskDto} from "../../shared/models/taskDto";

@Component({
  selector: 'app-search-reviewers',
  templateUrl: './search-reviewers.component.html',
  styleUrls: ['./search-reviewers.component.css']
})
export class SearchReviewersComponent implements OnInit {

  task: taskDto = new taskDto();

  allReviewers: Array<searchReviewerESDTO>;
  radioValue: string;
  justLoaded: boolean;
  selectedReviewers: Array<searchReviewerESDTO> = [];

  toasterConfig : ToasterConfig;
  seconds: string = "0";
  minutes: string = "0";
  hours: string = "0";
  days: string = "0";
  months: string = "0";

  constructor(private router:Router, private reviewerService: ReviewerService, private paperService: PaperService,
              private toasterService: ToasterService, private route: ActivatedRoute) {
    this.task.id = this.route.snapshot.paramMap.get('id');
    this.task.name = this.route.snapshot.paramMap.get('name');
    this.task.paperTitle = this.route.snapshot.paramMap.get('title');
    this.task.processId = this.route.snapshot.paramMap.get('processId');
    this.radioValue = "all";
    this.allReviewers = [];
    this.justLoaded = false;
  }

  ngOnInit() {
    this.getAllReviewers();
  }

  getAllReviewers(){
    this.reviewerService.getAll(this.task.paperTitle, this.task.processId).subscribe(data => {
      this.allReviewers = data;
      console.log(this.allReviewers);
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  filter(){
    this.justLoaded = true;
    this.allReviewers = [];
    if(this.radioValue == "fields")
      this.fieldsSearch();
    else if(this.radioValue == "moreLikeThis")
      this.moreLikeThis();
    else if(this.radioValue == "all")
      this.getAllReviewers();
    else
      this.geoSerach();
  }

  geoSerach(){
    this.reviewerService.geoSearch(this.task.paperTitle).subscribe(data => {
      this.allReviewers = data;
      console.log(this.allReviewers);
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  fieldsSearch(){
    this.reviewerService.fieldsSearch(this.task.paperTitle).subscribe(data => {
      this.allReviewers = data;
      console.log(this.allReviewers);
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  moreLikeThis(){
    this.reviewerService.moreLikeThisSearch(this.task.paperTitle).subscribe(data => {
      this.allReviewers = data;
      console.log(this.allReviewers);
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  addToChosen(reviewer){
    this.selectedReviewers.push(reviewer);
  }

  remove(reviewer){
    let i = this.selectedReviewers.indexOf(reviewer);
    this.selectedReviewers.splice(i, 1);
  }

  none(){
    this.reviewerService.setChiefReviewer(this.task.processId).subscribe(data => {
      this.toasterService.pop('success', 'Success', 'Editor in chief is choosing reviewers!');
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  choose(){
    let body = new FormData();
    let usernames = "";
    for(let r of this.selectedReviewers)
      usernames += r.username + ",";
    usernames = usernames.substring(0, usernames.length-1);
    body.append("usernames", usernames);
    body.append("sec", this.seconds);
    body.append("min", this.minutes);
    body.append("hours", this.hours);
    body.append("days", this.days);
    body.append("months", this.months);

    this.reviewerService.setReviewersToReview(this.task.processId, body).subscribe(data => {
      this.toasterService.pop('success', 'Success', 'Reviewers successfully chosen!');
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

}
