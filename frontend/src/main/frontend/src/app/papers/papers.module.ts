import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from "../shared/shared.module";
import {PapersRouterModule} from "./papers-router.module";
import {ToasterModule} from "angular5-toaster/dist";
import {SearchPapersComponent} from './search-papers/search-papers.component';
import { UploadPaperComponent } from './upload-paper/upload-paper.component';
import { ChooseJournalComponent } from './choose-journal/choose-journal.component';
import { PaperLookComponent } from './paper-look/paper-look.component';
import { ReuploadPaperComponent } from './reupload-paper/reupload-paper.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PapersRouterModule,
    ToasterModule
  ],
  declarations: [
    SearchPapersComponent,
    UploadPaperComponent,
    ChooseJournalComponent,
    PaperLookComponent,
    ReuploadPaperComponent
  ]
})
export class PapersModule { }
