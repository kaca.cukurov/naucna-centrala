import { Component, OnInit } from '@angular/core';
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {PaperService} from "../../core/services/paper.service";
import {ActivatedRoute, Router} from "@angular/router";
import {newSciencePaperDto} from "../../shared/models/newSciencePaperDto";
import {divAuthor} from "../../shared/models/divAuthor";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {AppError} from "../../shared/errors/app-error";

@Component({
  selector: 'app-upload-paper',
  templateUrl: './upload-paper.component.html',
  styleUrls: ['./upload-paper.component.css']
})
export class UploadPaperComponent implements OnInit {

  toasterConfig : ToasterConfig;
  fieldArray: Array<divAuthor> = [];
  newAttribute: divAuthor = new divAuthor();
  firstAuthor: divAuthor = new divAuthor();
  fileToUpload: File = null;
  processId: string;
  newPaper: newSciencePaperDto;

  allScienceFields: Array<Array<string>>;
  selectedScienceField: string;


  constructor(private router:Router, private paperService: PaperService, private toasterService: ToasterService,
              private route: ActivatedRoute) {
    this.toasterConfig = new ToasterConfig({timeout: 4000});
    this.allScienceFields = [["MECHANICAL_ENGINEERING", "Mechanical engineering"], ["COMPUTER_SCIENCE", "Computer science"],
      ["ROBOTICS_AND_AUTOMATION", "Robotics and automation"], ["GENERAL_MATHEMATICS", "General mathematics"],
      ["STATISTICS", "Statistics"]];
    this.newPaper = new newSciencePaperDto();
    this.selectedScienceField = this.allScienceFields[0][0];
    this.processId = this.route.snapshot.paramMap.get('id');
    this.newPaper.journalName = this.route.snapshot.paramMap.get('journal');

  }

  ngOnInit() {
  }

  handleFileInput(event: any) {
    this.fileToUpload = event.target.files[0];
  }

  upload(){
    let coauthors = '[';
    console.log(this.firstAuthor.firstName);
    if(this.firstAuthor.firstName){
      coauthors += '{'  + '\"firstName\":\"' + this.firstAuthor.firstName + '\",\"lastName\":\"' + this.firstAuthor.lastName
        + '\",\"city\":\"' + this.firstAuthor.city + '\",\"country\":\"' + this.firstAuthor.country +
        '\",\"email\":\"' + this.firstAuthor.email + '\"},';
      for(let row of this.fieldArray){
        coauthors += '{'  + '\"firstName\":\"' + row.firstName + '\",\"lastName\":\"' + row.lastName
          + '\",\"city\":\"' + row.city + '\",\"country\":\"' + row.country + '\",\"email\":\"' + row.email + '\"},';
      }
      coauthors = coauthors.substring(0, coauthors.length - 1);
    }

    coauthors += ']';
    let paper = '{' + '\"title\":\"' + this.newPaper.title + '\",\"coauthorDtos\":' + coauthors  +
      ',\"keyWords\":\"' + this.newPaper.keyWords + '\",\"paperAbstract\":\"' + this.newPaper.paperAbstract  +
      '\",\"scienceField\":\"' + this.selectedScienceField + '\",\"journalName\":\"' + this.newPaper.journalName + '\"}';

    let body = new FormData();
    body.append("file", this.fileToUpload);
    body.append("fileName", this.newPaper.title);
    console.log(paper);
    body.append("paperDto", paper);
    this.paperService.uploadPaper(body, this.processId).subscribe(data =>{
      this.toasterService.pop('success', 'success', data.success);
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'You can not upload that file');
        throw error;
      }
    });


  }

  addFieldValue() {
    this.fieldArray.push(this.newAttribute);
    this.newAttribute = new divAuthor();
  }

  deleteFieldValue(index) {
    this.fieldArray.splice(index, 1);
  }

}
