import { Component, OnInit } from '@angular/core';
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {PaperService} from "../../core/services/paper.service";
import {Router} from "@angular/router";
import {formFieldsDto} from "../../shared/models/FormFieldsDto";
import {AppError} from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {journalDto} from "../../shared/models/journalDto";
import {JournalService} from "../../core/services/journal.service";
import {formSubmissionDto} from "../../shared/models/formSubmissionDto";

@Component({
  selector: 'app-choose-journal',
  templateUrl: './choose-journal.component.html',
  styleUrls: ['./choose-journal.component.css']
})
export class ChooseJournalComponent implements OnInit {

  selectedJournal : journalDto;
  journals: Array<journalDto>;
  formFieldsDto: formFieldsDto;
  submissionForm: Array<formSubmissionDto> = [];
  toasterConfig : ToasterConfig;

  constructor(private router:Router, private paperService: PaperService, private journalService: JournalService,
              private toasterService: ToasterService) {
    this.toasterConfig = new ToasterConfig({timeout: 4000});
    this.journals = [];
  }

  ngOnInit() {
    this.startPublishingProcess();
    this.loadJournals();
  }

  choose(){
    let submission = new formSubmissionDto();
    submission.field = "id";
    submission.value = this.selectedJournal.id.toString();
    this.submissionForm.push(submission);
    this.paperService.chooseJournal(this.formFieldsDto.processInstanceId, this.submissionForm).subscribe(data => {
      if(data.taskId == "Unos podataka o radu"){
        this.router.navigate(["papers/upload", {id: data.processInstanceId, journal: this.selectedJournal.title}]);
      }
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Username exist', 'Change username!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  loadJournals(){
    this.journalService.getAll().subscribe(data => {
      this.journals = data;
      this.selectedJournal = this.journals[0];
      console.log(data);
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  startPublishingProcess(){
    this.paperService.startPublishingProcess().subscribe(data => {
      console.log("Started new process: " + data.processInstanceId);
      this.formFieldsDto = data;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

}
