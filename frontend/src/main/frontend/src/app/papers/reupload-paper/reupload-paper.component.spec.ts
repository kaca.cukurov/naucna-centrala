import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReuploadPaperComponent } from './reupload-paper.component';

describe('ReuploadPaperComponent', () => {
  let component: ReuploadPaperComponent;
  let fixture: ComponentFixture<ReuploadPaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReuploadPaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReuploadPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
