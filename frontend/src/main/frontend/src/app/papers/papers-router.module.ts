import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {SearchPapersComponent} from "./search-papers/search-papers.component";
import {UploadPaperComponent} from "./upload-paper/upload-paper.component";
import {ChooseJournalComponent} from "./choose-journal/choose-journal.component";
import {PaperLookComponent} from "./paper-look/paper-look.component";
import {ReuploadPaperComponent} from "./reupload-paper/reupload-paper.component";

const routes: Routes = [
  { path: 'search', component: SearchPapersComponent },
  { path: 'upload', component: UploadPaperComponent },
  { path: 'reupload', component: ReuploadPaperComponent },
  { path: 'publish', component: ChooseJournalComponent},
  { path: 'look', component: PaperLookComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PapersRouterModule { }
