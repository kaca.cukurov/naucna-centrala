import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {PaperService} from "../../core/services/paper.service";
import {taskDto} from "../../shared/models/taskDto";
import {AppError} from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {sciencePaperDto} from "../../shared/models/sciencePaperDto";
import {saveAs as importedSaveAs} from 'file-saver';
import {authorsDto} from "../../shared/models/authorsDto";
import {coauthorDTO} from "../../shared/models/coauthorDTO";

@Component({
  selector: 'app-paper-look',
  templateUrl: './paper-look.component.html',
  styleUrls: ['./paper-look.component.css']
})
export class PaperLookComponent implements OnInit {

  toasterConfig : ToasterConfig;
  task: taskDto = new taskDto();
  paper: sciencePaperDto;
  paperId: string;
  comment: string;
  sent : boolean;
  coauthors: Array<coauthorDTO>;
  seconds: string = "0";
  minutes: string = "0";
  hours: string = "0";
  days: string = "0";
  months: string = "0";
  format: string;

  constructor(private router:Router, private paperService: PaperService, private toasterService: ToasterService,
              private route: ActivatedRoute) {
    this.toasterConfig = new ToasterConfig({timeout: 4000});
    this.task.id = this.route.snapshot.paramMap.get('id');
    this.task.name = this.route.snapshot.paramMap.get('name');
    this.task.paperTitle = this.route.snapshot.paramMap.get('title');
    this.task.processId = this.route.snapshot.paramMap.get('processId');
    this.sent = false;
  }

  ngOnInit() {
    this.getPaper();
    this.getComment();
  }

  getComment(){
    this.paperService.getCoauthors(this.task.processId).subscribe(data =>{
      this.coauthors = data;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', '');
        throw error;
      }
    });
  }

  getPaper(){
    this.paperService.getByTitle(this.task.paperTitle).subscribe(data => {
      this.paper = data;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Username exist', 'Change username!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  decline(){
    this.paperService.declinePaper(this.task.processId).subscribe(data => {
      this.toasterService.pop('success', 'Success', 'You\'ve successfully declined paper!');
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Username exist', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  accept(){
    this.paperService.acceptPaper(this.task.processId).subscribe(data => {
      this.paperId = data;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Username exist', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  download(){
    this.paperService.downloadPdf(this.task.paperTitle).subscribe(
      blob => {
        importedSaveAs(blob, this.task.paperTitle + '.pdf');
      }, err => { });
  }

  sendBack(){
    let body = new FormData();
    body.append("comment", this.comment);
    body.append("sec", this.seconds);
    body.append("min", this.minutes);
    body.append("hours", this.hours);
    body.append("days", this.days);
    body.append("months", this.months);
    this.paperService.changeFormat(this.task.processId, body).subscribe(data => {
      this.sent = true;
      this.toasterService.pop('success', 'Success', 'You have successfully sent request to change format!');
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Username exist', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  review(){
    this.paperService.acceptFormat(this.task.processId).subscribe(() => {
      this.sent = true;
      this.toasterService.pop('success', 'Success', 'You have successfully sent this forward in process!');
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Username exist', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Ups, something gone wrong...');
        throw error;
      }
    });
  }

  getDuration(){
    this.format = "P";
    if(this.seconds != "")
      this.format += this.seconds + "S";
    if(this.minutes != "")
      this.format += this.minutes + "M";
    if(this.hours != "")
      this.format += this.hours + "H";
    if(this.days != "" || this.months != "")
      this.format += "T";
    if(this.days != "")
      this.format += this.days + "D";
    if(this.months != "")
      this.format += this.months + "M";
  }
}
