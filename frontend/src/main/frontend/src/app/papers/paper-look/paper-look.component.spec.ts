import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaperLookComponent } from './paper-look.component';

describe('PaperLookComponent', () => {
  let component: PaperLookComponent;
  let fixture: ComponentFixture<PaperLookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaperLookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaperLookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
