import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {PaperService} from "../../core/services/paper.service";
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {queryDTO} from "../../shared/models/queryDTO";
import {divPapers} from "../../shared/models/divPapers";
import {simpleQueryDTO} from "../../shared/models/simpleQueryDTO";
import {searchSciencePaperESDTO} from "../../shared/models/searchSciencePaperESDTO";
import {AppError} from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {saveAs as importedSaveAs} from 'file-saver';

@Component({
  selector: 'app-search-papers',
  templateUrl: './search-papers.component.html',
  styleUrls: ['./search-papers.component.css']
})
export class SearchPapersComponent implements OnInit {

  possibleFields: Map<string, string> = new Map();
  firstDiv: divPapers;

  fieldArray: Array<divPapers> = [];
  newAttribute: divPapers = new divPapers();
  queryDTO: queryDTO;

  toasterConfig : ToasterConfig;

  foundPapers: Array<searchSciencePaperESDTO>;

  phraseChecked: boolean;
  justLoaded: boolean;


  constructor(private router:Router, private paperService: PaperService, private toasterService: ToasterService) {
    this.toasterConfig = new ToasterConfig({timeout: 4000});
    this.firstDiv = new divPapers();
    this.possibleFields["Title"] = "title";
    this.possibleFields["Text"] = "text";
    this.possibleFields["Journal name"] = "journalName";
    this.possibleFields["Authors"] = "coauthors";
    this.possibleFields["Key words"] = "keyWords";
    this.possibleFields["Paper abstract"] = "paperAbstract";
    this.possibleFields["Science field"] = "scienceField";
    this.phraseChecked = false;
    this.foundPapers = [];
    this.justLoaded = false;
  }

  ngOnInit() {
  }

  search(){
    this.justLoaded = true;
    this.queryDTO = new queryDTO();
    let simpleQuery = new simpleQueryDTO;
    simpleQuery.field = this.possibleFields[this.firstDiv.firstField];
    simpleQuery.value = this.firstDiv.input;
    this.queryDTO.values.push(simpleQuery);
    for(let row of this.fieldArray){
      simpleQuery = new simpleQueryDTO;
      simpleQuery.field = this.possibleFields[row.firstField];
      simpleQuery.value = row.input;
      this.queryDTO.values.push(simpleQuery);
      this.queryDTO.operations.push(row.firstOperation);
    }
    this.queryDTO.phrase = this.phraseChecked;
    this.paperService.searchPapers(this.queryDTO).subscribe(data =>{
      for(let d of data)
        d.text = this.htmlToPlaintext(d.text);
      this.foundPapers = data;
      console.log(this.foundPapers);
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Error, look at console!');
        throw error;
      }
    });
  }

  savePDF(title){
    this.paperService.downloadPdf(title).subscribe(
      blob => {
        importedSaveAs(blob, title + '.pdf');
      }, err => { });
  }

  addFieldValue() {
    this.fieldArray.push(this.newAttribute);
    this.newAttribute = new divPapers();
  }

  deleteFieldValue(index) {
    this.fieldArray.splice(index, 1);
  }

  htmlToPlaintext(text) {
    return text ? String(text).replace(/<[^>]+>/gm, '') : '';
  }

}
