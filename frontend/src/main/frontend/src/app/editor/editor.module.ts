import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {ToasterModule} from "angular5-toaster/dist";
import {EditorRouterModule} from "./editor-router.module";
import {SharedModule} from "../shared/shared.module";
import { MakeDecisionComponent } from './make-decision/make-decision.component';
import { AfterChangeComponent } from './after-change/after-change.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    EditorRouterModule,
    ToasterModule
  ],
  declarations: [
    HomeComponent,
    MakeDecisionComponent,
    AfterChangeComponent
  ]
})
export class EditorModule { }
