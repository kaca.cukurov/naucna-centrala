import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {MakeDecisionComponent} from "./make-decision/make-decision.component";
import {AfterChangeComponent} from "./after-change/after-change.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'decide', component: MakeDecisionComponent },
  { path: 'after', component: AfterChangeComponent }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class EditorRouterModule { }
