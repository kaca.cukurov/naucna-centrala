import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {AppError} from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {taskDto} from "../../shared/models/taskDto";
import {ReviewerService} from "../../core/services/reviewer.service";
import {recomDto} from "../../shared/models/recomDto";
import {PaperService} from "../../core/services/paper.service";
import {saveAs as importedSaveAs} from 'file-saver';
import {authorsDto} from "../../shared/models/authorsDto";

@Component({
  selector: 'app-after-change',
  templateUrl: './after-change.component.html',
  styleUrls: ['./after-change.component.css']
})
export class AfterChangeComponent implements OnInit {

  toasterConfig : ToasterConfig;
  task: taskDto = new taskDto();

  recoms: Array<recomDto>;
  authors: authorsDto;
  seconds: string = "0";
  minutes: string = "0";
  hours: string = "0";
  days: string = "0";
  months: string = "0";

  constructor(private router:Router, private reviewerService: ReviewerService, private toasterService: ToasterService,
              private route: ActivatedRoute, private paperService: PaperService) {
    this.toasterConfig = new ToasterConfig({timeout: 4000});
    this.task.id = this.route.snapshot.paramMap.get('id');
    this.task.name = this.route.snapshot.paramMap.get('name');
    this.task.paperTitle = this.route.snapshot.paramMap.get('title');
    this.task.processId = this.route.snapshot.paramMap.get('processId');
  }

  ngOnInit() {
    this.loadRecoms();
    this.getComment();
  }

  getComment(){
    this.paperService.getComment(this.task.processId).subscribe(data =>{
      this.authors = data;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', '');
        throw error;
      }
    });
  }

  loadRecoms(){
    this.reviewerService.getRecoms(this.task.paperTitle).subscribe(data =>{
      this.recoms = data;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', '');
        throw error;
      }
    });
  }

  declineFinal(){
    let paperState = "denied";
    this.finalDecision(paperState);
  }

  changeAgain(){
    let paperState = "changeAgain";
    this.finalDecision(paperState);
  }

  acceptFinal(){
    let paperState = "accept";
    this.finalDecision(paperState);
  }

  finalDecision(state: string){
    let body = new FormData();
    body.append("paperState", state);
    body.append("sec", this.seconds);
    body.append("min", this.minutes);
    body.append("hours", this.hours);
    body.append("days", this.days);
    body.append("months", this.months);
    this.paperService.finalDecision(body, this.task.processId).subscribe(data => {
      this.toasterService.pop('success', 'Success', 'Decision made!');
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', '');
        throw error;
      }
    });
  }

  download(){
    this.paperService.downloadPdf(this.task.paperTitle).subscribe(
      blob => {
        importedSaveAs(blob, this.task.paperTitle + '.pdf');
      }, err => { });
  }

}
