import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfterChangeComponent } from './after-change.component';

describe('AfterChangeComponent', () => {
  let component: AfterChangeComponent;
  let fixture: ComponentFixture<AfterChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfterChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfterChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
