import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {AppError} from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {taskDto} from "../../shared/models/taskDto";
import {ReviewerService} from "../../core/services/reviewer.service";
import {recomDto} from "../../shared/models/recomDto";

@Component({
  selector: 'app-make-decision',
  templateUrl: './make-decision.component.html',
  styleUrls: ['./make-decision.component.css']
})
export class MakeDecisionComponent implements OnInit {


  toasterConfig : ToasterConfig;
  task: taskDto = new taskDto();

  recoms: Array<recomDto>;
  seconds: string = "0";
  minutes: string = "0";
  hours: string = "0";
  days: string = "0";
  months: string = "0";

  constructor(private router:Router, private reviewerService: ReviewerService, private toasterService: ToasterService,
              private route: ActivatedRoute) {
    this.toasterConfig = new ToasterConfig({timeout: 4000});
    this.task.id = this.route.snapshot.paramMap.get('id');
    this.task.name = this.route.snapshot.paramMap.get('name');
    this.task.paperTitle = this.route.snapshot.paramMap.get('title');
    this.task.processId = this.route.snapshot.paramMap.get('processId');
  }

  ngOnInit() {
    this.loadRecoms();
  }

  loadRecoms(){
    this.reviewerService.getRecoms(this.task.paperTitle).subscribe(data =>{
      this.recoms = data;
      console.log(this.recoms);
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'You can not upload that file');
        throw error;
      }
    });
  }

  acceptWithHugeChange(){
    let decision = "bigChange";
    this.makeDecision(decision);
  }

  acceptWithMinorChange(){
    let decision = "minorChange";
    this.makeDecision(decision);
  }

  accept(){
    let decision = "accept";
    this.makeDecision(decision);
  }

  makeDecision(decision: string){
    let body = new FormData();
    body.append("decision", decision);
    body.append("sec", this.seconds);
    body.append("min", this.minutes);
    body.append("hours", this.hours);
    body.append("days", this.days);
    body.append("months", this.months);
    this.reviewerService.decide(this.task.processId, body).subscribe(data => {
      this.toasterService.pop('success', 'Success', 'Decision made!');
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You do not have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'You can not upload that file');
        throw error;
      }
    });
  }

  reviewAgain(){
    let decision = "reviewAgain";
    this.makeDecision(decision);
  }

}
