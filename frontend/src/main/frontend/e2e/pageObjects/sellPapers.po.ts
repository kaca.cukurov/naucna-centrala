
import {by, element, ElementFinder} from "protractor";

export class SellPapersPage{

  getSubscribeButton():ElementFinder{
    return element(by.id('subscribeButton'));
  }

  getPlanButton(): ElementFinder{
    return element(by.id('planButton'));
  }

  getPapersTable(): ElementFinder{
    return element(by.id('paperTable'));
  }

  getEditionTable(): ElementFinder{
    return element(by.id('editionTable'));
  }

  getOnePaper(price: string): ElementFinder{
    return element(by.id('button' + price));
  }
}
