
import {by, element, ElementFinder} from "protractor";

export class HomePage{

  getBitcoinButton():ElementFinder{
    return element(by.id('bitcoinButton'));
  }

  getBankButton():ElementFinder{
    return element(by.id('bankButton'));
  }

  getPayPalButton():ElementFinder{
    return element(by.id('paypalButton'));
  }
}
