
import {by, element, ElementFinder} from "protractor";

export class BankPage{

  getHolderName():ElementFinder{
    return element(by.id('holderName'));
  }

  getPanNumber(): ElementFinder{
    return element(by.id('panNumber'));
  }

  getSecurityCode(): ElementFinder{
    return element(by.id('securityCode'));
  }

  getExpDate(): ElementFinder{
    return element(by.id('expDate'));
  }

  getSubmitButton(): ElementFinder{
    return element(by.id('submitButton'));
  }

  // set methods
  setHolderName(text: string) {
    this.getHolderName().sendKeys(text);
  }

  setPanNumber(text: string) {
    this.getPanNumber().sendKeys(text);
  }

  setExpDate(text: string) {
    this.getExpDate().sendKeys(text);
  }

  setSecurityCode(text: string) {
    this.getSecurityCode().sendKeys(text);
  }
}
