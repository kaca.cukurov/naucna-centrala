

import {HomePage} from "../pageObjects/home.po";
import {browser, by, element, protractor} from "protractor";
import {SellPapersPage} from "../pageObjects/sellPapers.po";
import {BankPage} from "../pageObjects/bank.po";
import {fakeAsync, inject, tick} from "@angular/core/testing";

describe('Bank payment', () => {

  let homePage: HomePage;
  let sellPapersPage: SellPapersPage;
  let bankPage: BankPage;
  let EC = protractor.ExpectedConditions;


  beforeEach(() => {
    sellPapersPage = new SellPapersPage();
    homePage = new HomePage();
    bankPage = new BankPage();

    browser.get('/selling');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/selling');

    expect(sellPapersPage.getEditionTable().isDisplayed()).toBe(true);
    expect(sellPapersPage.getPapersTable().isDisplayed()).toBe(true);
    expect(sellPapersPage.getPlanButton().isDisplayed()).toBe(true);
    expect(sellPapersPage.getSubscribeButton().isDisplayed()).toBe(true);
    expect(sellPapersPage.getOnePaper('5').isDisplayed()).toBe(true);

  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should successfully pay with credit card', () => {
    expect(sellPapersPage.getOnePaper('5').isEnabled()).toBe(true);

    browser.waitForAngularEnabled(false);

    sellPapersPage.getOnePaper('5').click();

    browser.wait(function() {
      // expect that browser redirect user to home
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:4200/home?customerId=-1&price=5&currency=USD&successUrl=%2F%2Flocalhost:4204%2Fselling&cancelUrl=%2F%2Flocalhost:4204%2Fselling';
      });
    }).then(function() {
      let elm =  homePage.getBankButton();
      browser.wait(EC.elementToBeClickable(elm), 5000);

      expect(homePage.getBankButton().isDisplayed()).toBe(true);
      expect(homePage.getBitcoinButton().isDisplayed()).toBe(true);
      expect(homePage.getPayPalButton().isDisplayed()).toBe(true);

      homePage.getBankButton().click();

      browser.wait(function(){
        return browser.getCurrentUrl().then(value => {
          let list = value.toString().split('/');
          let path = '';
          for(let i=0; i<list.length-1; i++){
            path += list[i] + '/';
          }
          return path === 'http://localhost:4201/payment/';
        });
      }).then(function () {

        expect(bankPage.getHolderName().isDisplayed()).toBe(true);
        expect(bankPage.getPanNumber().isDisplayed()).toBe(true);
        expect(bankPage.getSecurityCode().isDisplayed()).toBe(true);
        expect(bankPage.getExpDate().isDisplayed()).toBe(true);
        expect(bankPage.getSubmitButton().isDisplayed()).toBe(true);
        expect(bankPage.getSubmitButton().isEnabled()).toBe(false);

        bankPage.setHolderName("Paja Pajic");
        bankPage.setPanNumber("3259991222863138");
        bankPage.setSecurityCode("573");
        bankPage.setExpDate("12/23");
        expect(bankPage.getSubmitButton().isEnabled()).toBe(true);

        bankPage.getSubmitButton().click();

        browser.wait(function() {
          // expect that browser redirect user to success
          return browser.getCurrentUrl().then(value => {
            return value === 'http://localhost:4204/selling';
          });
        })
      });
    });
  });
});
